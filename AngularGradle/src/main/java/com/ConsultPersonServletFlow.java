package com;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.webflow.context.servlet.ServletExternalContext;
import org.springframework.webflow.execution.Action;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import com.journaldev.mongodb.dao.MongoDBPersonDAO;
import com.journaldev.mongodb.model.Person;
import com.mongodb.MongoClient;

@Service
public class ConsultPersonServletFlow implements Action
{
		public Event execute(RequestContext context)
		{
			String transition = null;
			ServletExternalContext externalContext = (ServletExternalContext) context.getExternalContext();
			HttpServletRequest request = (HttpServletRequest) externalContext.getNativeRequest();
			
			MongoClient mongo = (MongoClient) request.getServletContext()
					.getAttribute("MONGO_CLIENT");
			
			
			
			MongoDBPersonDAO personDAO = new MongoDBPersonDAO(mongo);
			List<Person> persons = personDAO.readAllPerson();
			request.setAttribute("persons", persons);

			 	String userName = "";
				String password = "";
				if(userName.equals("Chandan") && password.equals("TestPassword"))
				{
					transition = "true";
					return new Event(this, transition);
				}
				transition = "false";
				return new Event(this, transition);
				
		}
		
}
