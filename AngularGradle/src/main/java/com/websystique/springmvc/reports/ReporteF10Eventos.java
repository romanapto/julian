/**
 * 
 */
package com.websystique.springmvc.reports;

import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.websystique.springmvc.utils.EncabezadoSEC;
import com.websystique.springmvc.utils.ExcepcionRespuestaPdf;
import com.websystique.springmvc.utils.FontStyleSEC;
import com.websystique.springmvc.utils.RecuadroSEC;
import com.websystique.springmvc.utils.RoundBR;
import com.websystique.springmvc.utils.RoundRectangle;

/**
 * @author Indra. Clase para el manejo de eventos del formulario F10.
 */
public class ReporteF10Eventos extends ReportesSEC {
	private final static Logger LOGGER = Logger.getLogger(ReporteF10Eventos.class);
	private static final String TIPO_USUARIO = "1";
	private static final String LB_TITULO_ENCABEZADO = "Registro, Informe de Movimientos y/o Cancelaci�n Cuenta de Compensaci�n";
	private static final String LB_TEXTO_ENCABEZADO = "Circular Reglamentaria Externa DCIN-83 de ";
	private static final String LB_FORMULARIO_DIEZ = "Formulario No. 10";
	private static final String LB_46_PARA_USO_EXCLUSIVO = "Para uso exclusivo del Banco de la Rep�blica";
	private static final String FECHA_RADICACION = "Fecha Radicaci�n";
	private static final String NUMERO_RADICACION = "N�mero Radicaci�n";
	private static final String NUMERO_RADICACION_OPERACION = "El n�mero de radicaci�n de la operaci�n es: ";
	private static final String ESPACIO = " ";
	private String vrFechaEncabezado = "";
	private String vrNumeroENcabezado = "";
	private String vrUsoExclusivo = "";
	private String numeracionPagina = "P�gina %3d de";
	private String fechaCircular = "";
	private boolean esInterno;

	/** 
	 * Constructor ReporteF10Eventos
	 * @param vrFechaEncabezado
	 * @param vrNumeroENcabezado
	 * @param vrUsoExclusivo 
	 */
	public ReporteF10Eventos(String vrFechaEncabezado, String vrNumeroENcabezado, String vrUsoExclusivo, boolean esInterno) {
		super();
		this.vrFechaEncabezado = vrFechaEncabezado;
		this.vrNumeroENcabezado = vrNumeroENcabezado;
		this.vrUsoExclusivo = vrUsoExclusivo;
		this.esInterno = esInterno;
	}

	PdfTemplate total;

	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		total = writer.getDirectContent().createTemplate(30, 16);
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		try {
			this.generarEncabezado(writer);
			generarPiePaginaF10(writer, total);
		} catch (DocumentException e) {
			setExcepcionGenerada(e.getMessage());
			LOGGER.error("[ReporteF13][generarFormularioPDF] " + e.getMessage() + " Causa: " + e.getCause());
			ExceptionUtils.getStackTrace(e);
		} catch (MalformedURLException e) {
			setExcepcionGenerada(e.getMessage());
			LOGGER.error("[ReporteF13][generarFormularioPDF] " + e.getMessage() + " Causa: " + e.getCause());
			ExceptionUtils.getStackTrace(e);
		} catch (IOException e) {
			setExcepcionGenerada(e.getMessage());
			LOGGER.error("[ReporteF13][generarFormularioPDF] " + e.getMessage() + " Causa: " + e.getCause());
			ExceptionUtils.getStackTrace(e);
		}
	}

	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {
		ColumnText.showTextAligned(total, Element.ALIGN_LEFT, 
				new Phrase(String.valueOf(writer.getPageNumber() - 1)), 2, 1, 0);
	}
	
	/**
	 * Metodo encargado de generar el encabezado del formulario
	 * @throws DocumentException
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	public void generarEncabezado(PdfWriter writer) throws DocumentException, MalformedURLException, IOException {
		EncabezadoSEC encabezado = new EncabezadoSEC();
		encabezado.setTituloFormulario(null);
		
		RecuadroSEC datosRecuadro = new RecuadroSEC();
		datosRecuadro.setFechaRadicacion(cortarPalabra(vrFechaEncabezado, 16));
		datosRecuadro.setNumeroRadicacion(cortarPalabra(vrNumeroENcabezado, 16));
		encabezado.setRecuadro(datosRecuadro);
						
		encabezado.setTituloPrincipal(LB_TITULO_ENCABEZADO);		
		encabezado.setTituloSecundario(LB_TEXTO_ENCABEZADO + fechaCircular);		
		encabezado.setTituloFormulario(LB_FORMULARIO_DIEZ);
		crearEncabezadoF10(encabezado, writer);
	}
	
	/**
	 * Pie de pagina
	 * @throws DocumentException 
	 */
	protected void generarPiePaginaF10(PdfWriter writer, PdfTemplate total) throws DocumentException {
		crearPiePaginaF10(writer, total, LB_46_PARA_USO_EXCLUSIVO,cortarPalabra(vrUsoExclusivo.equalsIgnoreCase("-1") || vrUsoExclusivo.isEmpty() ? ESPACIO : NUMERO_RADICACION_OPERACION + vrUsoExclusivo, 80));
	}
	
	/**
	 * M�todo encargado de crear el encabezado del documento dependiendo del tipo de 
	 * usuario y el tipo de operaci�n para el formulario 10
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param writer 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	protected void crearEncabezadoF10(EncabezadoSEC encabezado, PdfWriter writer) 
			throws DocumentException, MalformedURLException, IOException {
		
		PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,63,30});
        outertable.setTotalWidth(527);
        outertable.setLockedWidth(false);
        outertable.getDefaultCell().setFixedHeight(20);
        
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario() != null ? 
        		encabezado.getTituloFormulario() : ESPACIO, FontStyleSEC.HELVETICA_BOLD_8PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulos(encabezado));
		
		RoundBR round = new RoundBR();
		round.setRadius(8);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable innertable = new PdfPTable(2);
		innertable.setWidths(new int[]{10,12});

		Phrase phrase = new Phrase(USO_EXCLUSIVO_BR, esInterno ? FontStyleSEC.HELVETICA_7PTS : 
			FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
		cell.setVerticalAlignment(Chunk.ALIGN_TOP);
		innertable.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		innertable.addCell(cell);

		phrase = new Phrase(esInterno ? FECHA_RADICACION : ESPACIO, FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getFechaRadicacion() != null && esInterno) ? 
				encabezado.getRecuadro().getFechaRadicacion() : ESPACIO);
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setColspan(1);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);
		
		phrase = new Phrase(esInterno ? NUMERO_RADICACION : ESPACIO, FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setColspan(1);
		cell.setBorder(Rectangle.NO_BORDER);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getNumeroRadicacion() != null && esInterno) ? 
				encabezado.getRecuadro().getNumeroRadicacion() : ESPACIO);
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);				

		cell = new PdfPCell(innertable);
		cell.setPadding(4);
		cell.setCellEvent(roundRectangle);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);

		outertable.addCell(cell);
		outertable.writeSelectedRows(0, -1, 34, 823, writer.getDirectContent());
	}
	
	/**
	 * Metodo encargado de crear el pie de pagina del formulario 10.
	 * @param writer Instancia del pdfWriter para el manejo de evento.
	 * @param total Numero de paginas total.
	 * @param label Titulo del campo.
	 * @param value Valor del campo.
	 * @throws DocumentException 
	 */
	protected void crearPiePaginaF10(PdfWriter writer, PdfTemplate total, String label, String value) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidths(new int[] { 98, 2 });
		table.setTotalWidth(523);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(-10);
		
		Phrase phrase = new Phrase(label, FontStyleSEC.HELVETICA_7PTS);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		phrase = new Phrase(value != "" ? value : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setFixedHeight(11.2f);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(Rectangle.BOX);
		table.addCell(cell);
		
		phrase = new Phrase(String.format(numeracionPagina, writer.getPageNumber()), FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		Image imagen = Image.getInstance(total);
		imagen.scalePercent(59F);
		cell = new PdfPCell(imagen);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setVerticalAlignment(Chunk.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		table.writeSelectedRows(0, -1, 36, 80, writer.getDirectContent());
	}

	@Override
	public String generarFormularioPDF() throws ExcepcionRespuestaPdf {
		// La implementacion de este metodo se encuentra en ReporteF10. 
		return null;
	}

	public String getFechaCircular() {
		return fechaCircular;
	}

	public void setFechaCircular(String fechaCircular) {
		this.fechaCircular = fechaCircular;
	}
	
	
}