package com.websystique.springmvc.reports;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.websystique.springmvc.utils.AlignTextSEC;
import com.websystique.springmvc.utils.BorderStyleSEC;
import com.websystique.springmvc.utils.CellSEC;
import com.websystique.springmvc.utils.CellValueSEC;
import com.websystique.springmvc.utils.CellValueTableSEC;
import com.websystique.springmvc.utils.Constantes;
import com.websystique.springmvc.utils.EncabezadoSEC;
import com.websystique.springmvc.utils.EncabezadoTableSEC;
import com.websystique.springmvc.utils.ExcepcionRespuestaPdf;
import com.websystique.springmvc.utils.FontStyleSEC;
import com.websystique.springmvc.utils.RecuadroUtil;
import com.websystique.springmvc.utils.RoundBR;
import com.websystique.springmvc.utils.RoundRectangle;
import com.websystique.springmvc.utils.RowTableSEC;
import com.websystique.springmvc.utils.TableSEC;


public abstract class ReportesSEC extends PdfPageEventHelper {
	/** Log de la clase */
	private final static Logger LOG = Logger.getLogger(ReportesSEC.class);
	/**
	 * Objeto sobre el cual se generar� el documento PDF.
	 */
	protected Document documentPDF; 
	
	/**
	 * Atributo que contiene la imagen del LOGO del BR.
	 */
	private static Image logoBR;
	
	private static String pathLogo;
	
	protected String pathPDF = "";
	
	protected static final String MSJ_ERROR_REPORTE = "Ha ocurrido un error al realizar la generaci�n del formulario. Por favor comunicarse con la mesa de ayuda para solucionar el inconveniente.";
	
	protected static final String USO_EXCLUSIVO_BR = "USO EXCLUSIVO DEL BANCO DE LA REP�BLICA";
	
	protected static final String NUM_OPERACION = "N�MERO DE OPERACI�N";
	
	private static final float ALTURA_DEFAULT = 0f;
	
	protected float alturaCelda = ALTURA_DEFAULT;
	
	
	protected String excepcionGenerada = null;
	
	public ReportesSEC() {
		documentPDF = new Document();
	}
	
	/**
	 * M�todo para definir las m�rgenes del documento PDF en base de los parametros del constructor.
	 * 
	 * @param margenIzquierda  N�mero margen izquierda del documento
	 * @param margenDerecha N�mero margen derecha del documento
	 * @param margenArriba N�mero margen superior. En caso de que el documento PDF no genere hojas anexas, debe ser igual a 0.
	 * @param margenAbajo N�mero margen inferior. En caso de que el documento PDF no genere hojas anexas, debe ser igual a 0.
	 */
	public ReportesSEC(String margenIzquierda, String margenDerecha, String margenArriba , String margenAbajo) {
		documentPDF = new Document(PageSize.A4 , Float.parseFloat(margenIzquierda) , Float.parseFloat(margenDerecha) , Float.parseFloat(margenArriba) , Float.parseFloat(margenAbajo));
	}
	
	
	/**
	 * M�todo encargado de crear el encabezado del documento dependiendo del tipo de 
	 * usuario y el tipo de operaci�n
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param writer 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	protected void crearEncabezado(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion) throws MalformedURLException, IOException, DocumentException {
			if(encabezado.getTipoUsuario() == 1){
				encabezadoUsuarioInterno(encabezado, cuadroExtendido, recuadroSoloOperacion);	
			}else if(encabezado.getTipoUsuario() == 2){
				encabezadoUsuarioExterno(encabezado, cuadroExtendido);
			}else if(encabezado.getTipoUsuario() == 3){
				encabezadoUsuarioSinAutenticacion(encabezado);
			} else{
				encabezadoUsuarioSinAutenticacion(encabezado);
			}
	}
	
	/**
	 * crearEncabezadoDinamico
	 * @param encabezado
	 * @param cuadroExtendido
	 * @param recuadroSoloOperacion
	 * @param writer
	 * @param titleEnvioExterno
	 * @throws MalformedURLException
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected void crearEncabezadoDinamico(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion, PdfWriter writer, String titleEnvioExterno) throws MalformedURLException, DocumentException, IOException {
		LOG.debug("[ReportesSEC][crearEncabezadoDinamico][INICIO]");
		if(encabezado.getTipoUsuario() == 1){
			encabezadoUsuarioInternoDinamico(encabezado, cuadroExtendido, recuadroSoloOperacion, writer);	
		}else if(encabezado.getTipoUsuario() == 2){
			encabezadoUsuarioExternoDinamico(encabezado, cuadroExtendido, writer, titleEnvioExterno);
		}else if(encabezado.getTipoUsuario() == 3){
			encabezadoUsuarioSinAutenticacionDinamico(encabezado, writer);
		} else{
			encabezadoUsuarioSinAutenticacionDinamico(encabezado, writer);
		}
		LOG.debug("[ReportesSEC][crearEncabezadoDinamico][FIN]");
	}
	
	
	protected void crearEncabezadoF3A(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion) throws MalformedURLException, IOException, DocumentException {
			if(encabezado.getTipoUsuario() == 1){
				encabezadoUsuarioInternoF3A(encabezado, cuadroExtendido, recuadroSoloOperacion);	
			}else if(encabezado.getTipoUsuario() == 2){
				encabezadoUsuarioExternoF3A(encabezado, cuadroExtendido);
			}else if(encabezado.getTipoUsuario() == 3){
				encabezadoUsuarioSinAutenticacionF3A(encabezado);
			} else{
				encabezadoUsuarioSinAutenticacionF3A(encabezado);
			}
	}
	
	protected void crearEncabezadoF3(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion) throws MalformedURLException, IOException, DocumentException {
		if(encabezado.getTipoUsuario() == 1){
			encabezadoUsuarioInternoF3(encabezado, cuadroExtendido, recuadroSoloOperacion);	
		}else if(encabezado.getTipoUsuario() == 2){
			encabezadoUsuarioExternoF3(encabezado, cuadroExtendido);
		}else if(encabezado.getTipoUsuario() == 3){
			encabezadoUsuarioSinAutenticacionF3(encabezado);
		} else{
			encabezadoUsuarioSinAutenticacionF3(encabezado);
		}
	}

	protected void crearEncabezadoF4(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException {
		PdfPTable outertable = generarImagenTitulosF4(encabezado);
		outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));
		documentPDF.add(outertable);
		
	}
	
	/**
	 * M�todo encargado de insertar la imagen de logo del Banco de La Rep�blica en el documento
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado
	 * @return Objeto de tipo PDFPCell que contiene una celda con la imagen insertada
	 * @throws BadElementException Excepci�n generada 
	 * @throws MalformedURLException Excepci�n generada en caso que la ruta de la imagen no sea correcta
	 * @throws IOException Excepci�n generada en caso que se presente un problema con el archivo de la imagen
	 */
	protected PdfPCell generarImagen(EncabezadoSEC encabezado) throws BadElementException, MalformedURLException, IOException{
        Image foto = getLogoBR();		
		foto.scaleToFit(40, 40);
        
		//imagen
        PdfPCell cell = new PdfPCell(foto);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(4);
        cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
        return cell;
	}
	
	/**
	 * M�todo encargado de generar los t�tulos del encabezado
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado
	 * @return Objeto celda con los t�tulos
	 */
	protected PdfPCell generarTitulos(EncabezadoSEC encabezado){
		 //titulo
        PdfPTable tablaTitulos = new PdfPTable(1);
        tablaTitulos.setWidthPercentage(60);

        Phrase phrase = new Phrase(encabezado.getTituloPrincipal(), FontStyleSEC.TIMES_ROMAN_BOLD_9PTS);
        PdfPCell cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
        tablaTitulos.addCell(cell);
        
        phrase = new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario():"", FontStyleSEC.TIMES_ROMAN_BOLD_9PTS);
        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
        tablaTitulos.addCell(cell);
        
        phrase = new Phrase(encabezado.getTituloSecundario(), FontStyleSEC.TIMES_ROMAN_9PTS);
        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
        tablaTitulos.addCell(cell);
        
        cell = new PdfPCell(tablaTitulos);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(4);
        cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
        return cell;
	}
	
	private PdfPCell generarTitulosF4(EncabezadoSEC encabezado){
		 //titulo
       PdfPTable tablaTitulos = new PdfPTable(1);
       tablaTitulos.setWidthPercentage(60);

       Phrase phrase = new Phrase(encabezado.getTituloPrincipal(), FontStyleSEC.TIMES_ROMAN_BOLD_9PTS);
       PdfPCell cell = new PdfPCell(phrase);
       cell.setBorder(Rectangle.NO_BORDER);
       cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
       tablaTitulos.addCell(cell);
             
       cell = new PdfPCell(tablaTitulos);
       cell.setBorder(Rectangle.NO_BORDER);
       cell.setPadding(4);
       cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
       return cell;
	}
	
	
	
	private PdfPCell generarTitulosF3A(EncabezadoSEC encabezado){
		 //titulo
       PdfPTable tablaTitulos = new PdfPTable(1);
       tablaTitulos.setWidthPercentage(60);

       Phrase phrase = new Phrase(encabezado.getTituloPrincipal(), FontStyleSEC.TIMES_ROMAN_BOLD_9PTS);
       PdfPCell cell = new PdfPCell(phrase);
       cell.setBorder(Rectangle.NO_BORDER);
       cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
       tablaTitulos.addCell(cell);
       
       phrase = new Phrase(encabezado.getTituloSecundario(), FontStyleSEC.TIMES_ROMAN_9PTS);
       cell = new PdfPCell(phrase);
       cell.setBorder(Rectangle.NO_BORDER);
       cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
       tablaTitulos.addCell(cell);
             
       cell = new PdfPCell(tablaTitulos);
       cell.setBorder(Rectangle.NO_BORDER);
       cell.setPadding(4);
       cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
       return cell;
	}
	
	private PdfPCell generarTitulosF3(EncabezadoSEC encabezado){
		 //titulo
      PdfPTable tablaTitulos = new PdfPTable(1);
      tablaTitulos.setWidthPercentage(60);

      Phrase phrase = new Phrase(encabezado.getTituloPrincipal(), FontStyleSEC.TIMES_ROMAN_BOLD_9PTS);
      PdfPCell cell = new PdfPCell(phrase);
      cell.setBorder(Rectangle.NO_BORDER);
      cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
      tablaTitulos.addCell(cell);
            
      cell = new PdfPCell(tablaTitulos);
      cell.setBorder(Rectangle.NO_BORDER);
      cell.setPadding(4);
      cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
      return cell;
	}
	
	
	/**
	 * M�todo encargado de generar el encabezado para el usuario interno
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void encabezadoUsuarioInterno(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion) throws MalformedURLException, IOException, DocumentException{
		  PdfPTable outertable = generarImagenTitulos(encabezado);
		  if(encabezado.getTipoOperacion() == 0){//USUARIO INTERNO - SIN OPERACION
	        	outertable.addCell(RecuadroUtil.generarRecuadroInternoSinOperacion(encabezado, 2, new int[]{17,20}, USO_EXCLUSIVO_BR));
	        }else if(encabezado.getTipoOperacion() == 1){//USUARIO INTERNO - OPERACION INICIAL		        		        	
	        	if(cuadroExtendido){
		        	outertable.addCell(RecuadroUtil.generarRecuadroInternoConOperacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));			        	
		        }else if (recuadroSoloOperacion){
		        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{25}, NUM_OPERACION));
	        	}else{
	        		outertable.addCell(RecuadroUtil.generarRecuadroInternoInicial(encabezado, 2, new int[]{17,20}, USO_EXCLUSIVO_BR));
	        	}		        	
	        }else if(encabezado.getTipoOperacion() == 2 || encabezado.getTipoOperacion() == 3 || encabezado.getTipoOperacion() == 4){ //USUARIO INTERNO - OPERACION MODICAR O ANULAR
	        	if(cuadroExtendido){
		        	outertable.addCell(RecuadroUtil.generarRecuadroInternoConOperacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));			        	
		        }else if (recuadroSoloOperacion){
		        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{25}, NUM_OPERACION));
	        	}else{
	        		outertable.addCell(RecuadroUtil.generarRecuadroInternoModificacionAnulacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));
	        	}
	        }
	        documentPDF.add(outertable);	  
	}
	
	
	private void encabezadoUsuarioInternoF3A(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion) throws MalformedURLException, IOException, DocumentException{
		  PdfPTable outertable = generarImagenTitulosF3A(encabezado);
		  if(encabezado.getTipoOperacion() == 0){//USUARIO INTERNO - SIN OPERACION
	        	outertable.addCell(RecuadroUtil.generarRecuadroInternoSinOperacion(encabezado, 2, new int[]{17,20}, USO_EXCLUSIVO_BR));
	        }else if(encabezado.getTipoOperacion() == 1){//USUARIO INTERNO - OPERACION INICIAL		        		        	
	        	if(cuadroExtendido){
		        	outertable.addCell(RecuadroUtil.generarRecuadroInternoConOperacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));			        	
		        }else if (recuadroSoloOperacion){
		        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{25}, NUM_OPERACION));
	        	}else{
	        		outertable.addCell(RecuadroUtil.generarRecuadroInternoInicial(encabezado, 2, new int[]{17,20}, USO_EXCLUSIVO_BR));
	        	}		        	
	        }else if(encabezado.getTipoOperacion() == 2 || encabezado.getTipoOperacion() == 3 || encabezado.getTipoOperacion() == 4){ //USUARIO INTERNO - OPERACION MODICAR O ANULAR
	        	if(cuadroExtendido){
		        	outertable.addCell(RecuadroUtil.generarRecuadroInternoConOperacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));			        	
		        }else if (recuadroSoloOperacion){
		        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{25}, NUM_OPERACION));
	        	}else{
	        		outertable.addCell(RecuadroUtil.generarRecuadroInternoModificacionAnulacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));
	        	}
	        }
	        documentPDF.add(outertable);	  
	}
	
	private void encabezadoUsuarioInternoF3(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion) throws MalformedURLException, IOException, DocumentException{
		  PdfPTable outertable = generarImagenTitulosF3(encabezado);
		  if(encabezado.getTipoOperacion() == 0){//USUARIO INTERNO - SIN OPERACION
	        	outertable.addCell(RecuadroUtil.generarRecuadroInternoSinOperacion(encabezado, 2, new int[]{17,20}, USO_EXCLUSIVO_BR));
	        }else if(encabezado.getTipoOperacion() == 1){//USUARIO INTERNO - OPERACION INICIAL		        		        	
	        	if(cuadroExtendido){
		        	outertable.addCell(RecuadroUtil.generarRecuadroInternoConOperacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));			        	
		        }else if (recuadroSoloOperacion){
		        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{25}, NUM_OPERACION));
	        	}else{
	        		outertable.addCell(RecuadroUtil.generarRecuadroInternoInicial(encabezado, 2, new int[]{17,20}, USO_EXCLUSIVO_BR));
	        	}		        	
	        }else if(encabezado.getTipoOperacion() == 2 || encabezado.getTipoOperacion() == 3 || encabezado.getTipoOperacion() == 4){ //USUARIO INTERNO - OPERACION MODICAR O ANULAR
	        	if(cuadroExtendido){
		        	outertable.addCell(RecuadroUtil.generarRecuadroInternoConOperacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));			        	
		        }else if (recuadroSoloOperacion){
		        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{25}, NUM_OPERACION));
	        	}else{
	        		outertable.addCell(RecuadroUtil.generarRecuadroInternoModificacionAnulacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));
	        	}
	        }
	        documentPDF.add(outertable);	  
	}
	
	
	/**
	 * M�todo encargado de generar el encabezado para el usuario externo
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void encabezadoUsuarioExterno(EncabezadoSEC encabezado, boolean cuadroExtendido) throws MalformedURLException, IOException, DocumentException{
        	 PdfPTable outertable = generarImagenTitulos(encabezado);
        	 if(encabezado.getTipoOperacion() == 1){ //USUARIO EXTERNO - OPERACION INICIAL	        		 
        		 if(cuadroExtendido){
		        		outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));
		         }else{
		        		outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));			        	
			     }		        		 	 	        		
 	        }else if(encabezado.getTipoOperacion() == 2){ //USUARIO EXTERNO - MODIFICACION
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }else if(encabezado.getTipoOperacion() == 0){ //SOLO IMPRESION SIN SELECCIONAR TIPO OPERACION
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }else if(encabezado.getTipoOperacion() == 4){ //MODIFICACION (PARA EL F3)
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }
        	documentPDF.add(outertable);
	}
	
	private void encabezadoUsuarioExternoF3A(EncabezadoSEC encabezado, boolean cuadroExtendido) throws MalformedURLException, IOException, DocumentException{
        	 PdfPTable outertable = generarImagenTitulosF3A(encabezado);
        	 if(encabezado.getTipoOperacion() == 1){ //USUARIO EXTERNO - OPERACION INICIAL	        		 
        		 if(cuadroExtendido){
		        		outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));
		         }else{
		        		outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));			        	
			     }		        		 	 	        		
 	        }else if(encabezado.getTipoOperacion() == 2){ //USUARIO EXTERNO - MODIFICACION
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }else if(encabezado.getTipoOperacion() == 0){ //SOLO IMPRESION SIN SELECCIONAR TIPO OPERACION
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }else if(encabezado.getTipoOperacion() == 4){ //MODIFICACION (PARA EL F3)
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }
        	documentPDF.add(outertable);
	}
		
	
	private void encabezadoUsuarioExternoF3(EncabezadoSEC encabezado, boolean cuadroExtendido) throws MalformedURLException, IOException, DocumentException{
        	 PdfPTable outertable = generarImagenTitulosF3(encabezado);
        	 if(encabezado.getTipoOperacion() == 1){ //USUARIO EXTERNO - OPERACION INICIAL	        		 
        		 if(cuadroExtendido){
		        		outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));
		         }else{
		        		outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));			        	
			     }		        		 	 	        		
 	        }else if(encabezado.getTipoOperacion() == 2){ //USUARIO EXTERNO - MODIFICACION
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }else if(encabezado.getTipoOperacion() == 0){ //SOLO IMPRESION SIN SELECCIONAR TIPO OPERACION
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }else if(encabezado.getTipoOperacion() == 4){ //MODIFICACION (PARA EL F3)
 	        	outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, USO_EXCLUSIVO_BR));	
 	        }
        	documentPDF.add(outertable);
	}
	
	/**
	 * M�todo encargado de generar el encabezado para el usuario sin autenticar
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void encabezadoUsuarioSinAutenticacion(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
			PdfPTable outertable = generarImagenTitulos(encabezado);
			outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));
			documentPDF.add(outertable);
	}
	
	private void encabezadoUsuarioSinAutenticacionF3A(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
			PdfPTable outertable = generarImagenTitulos(encabezado);
			outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));
			documentPDF.add(outertable);
	}
	
	private void encabezadoUsuarioSinAutenticacionF3(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
			PdfPTable outertable = generarImagenTitulos(encabezado);
			outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));
			documentPDF.add(outertable);
	}
	
	/**
	 * M�todo encargado de organizar el logo y los t�tulos dentro del documento
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado
	 * @return Un objeto tabla con los titulos y el logo
	 * @throws MalformedURLException Excepci�n generada en caso que la ruta del archivo tenga problemas
	 * @throws IOException Excepci�n generada en caso que se presente un problema con el archivo de la imagen
	 * @throws DocumentException Expci�n generada en caso que se presente un problema en la creaci�n del documento
	 */
	private PdfPTable generarImagenTitulos(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
	 	PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,58,35});
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario():"", FontStyleSEC.HELVETICA_BOLD_8PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulos(encabezado));
        return outertable;
	}
	
	private PdfPTable generarImagenTitulosF4(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
	 	PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,48,45});
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario():"", FontStyleSEC.HELVETICA_BOLD_8PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulosF4(encabezado));
        return outertable;
	}
	
	private PdfPTable generarImagenTitulosF3A(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
	 	PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,58,35});
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario():"", FontStyleSEC.HELVETICA_BOLD_8PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulosF3A(encabezado));
        return outertable;
}
	
	private PdfPTable generarImagenTitulosF3(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
	 	PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,58,35});
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario():"", FontStyleSEC.HELVETICA_BOLD_8PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulosF3(encabezado));
        return outertable;
}
	
	/**
	 * M�todo en el cual se va a generar el documento PDF.
	 * @return String Ruta donde se gener� el PDF en el servidor.
	 */
	public abstract String generarFormularioPDF() throws ExcepcionRespuestaPdf;
	
	
	/**
	 * M�todo en el cual se crear� un t�tulo en mayuscula
	 * y en negrilla, basandose en el texto que se encuentra en
	 * el atributo value del objeto CellValueSEC pasado como
	 * par�metro.
	 * Si el atributo estilo(FontStyleSEC) del objeto CellValueSEC es nulo,
	 * se crea el t�tulo con un estilo en negrita y en mayuscula.
	 * Si el atributo align(AlignTextSEC) del objeto CellValueSEC es nulo,
	 * se realiza la alineaci�n a la izquierda por defecto.
	 * Si los anteriores atributos, son diferentes de nulos, se debe crear
	 * el t�tulo con la informaci�n de estilo y alineaci�n que se encuentre en ellos.
	 * @param titulo CellValueSEC Objeto con la informaci�n del t�tulo.
	 */
	protected PdfPCell addTitulo(CellValueSEC titulo) {
		Phrase texto = new Phrase(titulo.getValue().toString().toUpperCase());
		texto.getFont().setStyle(titulo.getEstilo() == null?FontStyleSEC.TIMES_ROMAN_BOLD_7PTS.getFamilyname():titulo.getEstilo().getFamilyname());
		PdfPCell celda = new PdfPCell(texto);
		celda.setHorizontalAlignment(titulo.getAlign());
		celda.setBorder(BorderStyleSEC.NO_BORDER);
		return celda;
	}
	
	
	/**
	 * M�todo que genera un titulo y en la misma fila adiciona las celdas
	 * pasadas en la lista como parametro, teniendo en cuenta el valor
	 * del parametro separacion �l cu�l es el valor de separaci�n entre
	 * el texto del t�tulo y la primera casilla de la lista pasada como parametros.
	 * @param titulo CellValueSEC objeto con la informaci�n del t�tulo.
	 * @param separacion int valor de separaci�n entre el titulo y las casillas.
	 * @param lstCeldas Listado de objetos de tipo CellSec, con las casillas adicionales
	 * que deben ir en la misma fila del t�tulo.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	protected void addTitulo(CellValueSEC titulo, int separacion , List<CellSEC> lstCeldas, float ancho) throws DocumentException, MalformedURLException, IOException {
		// Paso 1 - Invocar m�todo crearTitulo
		int columnas = lstCeldas.size()+1;
		PdfPTable tablaSeccion1 = new PdfPTable(columnas);
		tablaSeccion1.addCell(addTitulo(titulo));
			for(int i=0; i <lstCeldas.size(); i++){
				tablaSeccion1.addCell(this.addFila((CellSEC)lstCeldas.get(i), ancho));
			}
			documentPDF.add(tablaSeccion1);
	}
	
	/**
	 * M�todo encargado de agregar una lista de celdas al documento pdf
	 * @param lstCeldas Lista de celdas a agregar
	 * @param lstAnchos Ancho de cada una de las celdas a agregar
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	
	protected void addFila(List<CellSEC> lstCeldas, float[] lstAnchos, float anchoPorcentaje) throws DocumentException, MalformedURLException, IOException {
		PdfPTable tabla = new PdfPTable(lstCeldas.size());
		tabla.setWidths(lstAnchos);
		tabla.setWidthPercentage(anchoPorcentaje);
		PdfPCell cellTmp;
		for(int i=0; i < lstCeldas.size(); i++){
			cellTmp = new PdfPCell(this.addFila(lstCeldas.get(i), anchoPorcentaje));
			cellTmp.setBorder(BorderStyleSEC.NO_BORDER);
			tabla.addCell(cellTmp);
		}
		documentPDF.add(tabla);
	}
	
	
	/**
	 * M�todo encargado de agregar una celda al formulario
	 * @param cellSec objeto con los datos de formato y contenido de la celda
	 * @return Una celda IText
	 * @throws DocumentException Excepci�n generada al agregar celdadas al documento
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	
	protected PdfPCell addFila(CellSEC cellSec, float ancho) throws DocumentException, MalformedURLException, IOException{
		
		
		PdfPTable tabla = new PdfPTable(1);
		tabla.setWidthPercentage(ancho);
		if(cellSec.getLabel() != null){
			tabla.addCell(crearCelda(cellSec.getLabel()));
		}
		tabla.addCell(crearCelda(cellSec.getValue()));
		PdfPCell pdfCellRetorno=new PdfPCell(tabla);	
		pdfCellRetorno.setPadding(0);
		pdfCellRetorno.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
		return pdfCellRetorno;		
	}
	

	/**
	 * M�todo encargado de crear una celda IText
	 * @param cellValueSec objeto con los datos de formato de la celda a crear
	 * @return Objeto con el formato y los datos recibidos en el par�metro de entrada
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws BadElementException 
	 */
	private PdfPCell crearCelda(CellValueSEC cellValueSec) throws BadElementException, MalformedURLException, IOException {
		PdfPCell pdfCell = null;
		if(!cellValueSec.isImagen()){
			Paragraph phrase = new Paragraph((cellValueSec.getValue()==null || cellValueSec.getValue().toString().equals(""))?" ":cellValueSec.getValue().toString(), cellValueSec.getEstilo());
			pdfCell = new PdfPCell(phrase);
		}else{
			Image foto = Image.getInstance(cellValueSec.getValue().toString());	
		    foto.scaleToFit(cellValueSec.getFit()[0], cellValueSec.getFit()[1]);
		    pdfCell = new PdfPCell(foto);
			    
		}
		
		pdfCell.setNoWrap(cellValueSec.isNoWrap());
		pdfCell.setHorizontalAlignment(cellValueSec.getAlign());
		pdfCell.setBorder(cellValueSec.getBorderStyle());
		pdfCell.setRowspan(cellValueSec.getRowSpan());
		pdfCell.setVerticalAlignment(cellValueSec.getVerticalAlign());
		pdfCell.setPadding(cellValueSec.getPadding());
		pdfCell.setColspan(cellValueSec.getColSpan());
		
		if (getAlturaCelda() > ALTURA_DEFAULT) {
			pdfCell.setFixedHeight(getAlturaCelda());
		}
		
		return pdfCell;
	}
		
	/**
	 * M�todo encargado de generar una fila v�cia de un tama�o especificado por el usuario
	 * @param alto valor que ser� asignado como alto de la fila
	 * @throws DocumentException Excepci�n generada en caso que se presente problema al agregar al documento
	 */
	protected void addFilaVacia(float alto) throws DocumentException{
		PdfPTable tabla = new PdfPTable(1); 
		tabla.setWidthPercentage(100);
		PdfPCell celdaVacia = new PdfPCell(new Phrase(" "));
		celdaVacia.setPadding(0);
		celdaVacia.setBorder(BorderStyleSEC.NO_BORDER);
		celdaVacia.setFixedHeight(alto);
		tabla.addCell(celdaVacia);
		documentPDF.add(tabla);
	}
	
	
	/**
	 * M�todo encargado de agregar una lista de celdas
	 * @param lstTitulos Lista de los valores que van dentro de las celdas
	 * @param estilo Estilo de la letra
	 * @param borde Borde de la celda
	 * @return Lista de CellSEC creadas
	 */
	protected ArrayList<CellSEC> agregarValores(List<String> lstTitulos, Font estilo, int borde){
		ArrayList<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		CellSEC cellTmp;
		CellValueSEC valueTmp;
		for(int i=0; i < lstTitulos.size(); i++){
			cellTmp = new CellSEC();
			valueTmp = new CellValueSEC(estilo, borde, lstTitulos.get(i));
			cellTmp.setValue(valueTmp);
			lstCeldas.add(cellTmp);
		}
		return lstCeldas;
	}
			
	/**
	 * M�todo encargado de agregar una lista de celdas
	 * @param lstTitulos Lista de los valores que van dentro de las celdas
	 * @param estilo Estilo de la letra
	 * @param borde Borde de la celda
	 * @param lstAlign Lista con las alineaciones de el PDF
	 * @return Lista de CellSEC creadas
	 */
	protected ArrayList<CellSEC> agregarValores(List<String> lstTitulos, Font estilo, int borde, List<String> lstAlign){
		ArrayList<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		CellSEC cellTmp;
		CellValueSEC valueTmp;
		for(int i=0; i < lstTitulos.size(); i++){
			cellTmp = new CellSEC();
			valueTmp = new CellValueSEC(estilo, Integer.parseInt(lstAlign.get(i).toString()), borde, lstTitulos.get(i));
			cellTmp.setValue(valueTmp);
			lstCeldas.add(cellTmp);
		}
		return lstCeldas;
	}
			
	/**
	 * M�todo encargado de agregar un parrafo al formulario
	 * @param cell Obejto que con el contenido y el formato del parrafo
	 * @param interlineado espacio entre las l�neas del parrafo
	 * @throws DocumentException 
	 */
	protected void addParagraph(CellValueSEC cell, float interlineado) throws DocumentException{
		Paragraph parrafo = new Paragraph(interlineado);
		parrafo.setAlignment(cell.getAlign());
	    Phrase texto = new Phrase(cell.getValue().toString(), cell.getEstilo());
	    parrafo.add(texto);
		documentPDF.add(parrafo);
	}
	
	/**
	 * M�todo encargado de generar los encabezados de la tabla
	 * @param encabezadosTabla Objeto de tipo lista con los encabezados de la tabla
	 * @param tablaPdf tabla a la que se le deben agregar los encabezados
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws BadElementException 
	 */
	private void generarEncabezadosTabla(List<EncabezadoTableSEC> encabezadosTabla, PdfPTable tablaPdf) throws BadElementException, MalformedURLException, IOException{
			List<PdfPCell> lstTmp = new ArrayList<PdfPCell>();
			for(int i=0; i < encabezadosTabla.size(); i++){
				CellValueTableSEC cellTmp;
				EncabezadoTableSEC encabezado = (EncabezadoTableSEC) encabezadosTabla.get(i);
				if (encabezado.getEncabezado().getTitulosTabla().size() >  encabezado.getEncabezado().getRowSpan()){
					PdfPCell celda = crearCelda(new CellValueTableSEC(encabezado.getEncabezado().getEstilo(), encabezado.getEncabezado().getAlign(), encabezado.getEncabezado().getBorderStyle(), encabezado.getEncabezado().getVerticalAlign(), encabezado.getEncabezado().getRowSpan(), encabezado.getEncabezado().getTitulosTabla().get(0)));
					tablaPdf.addCell(celda);
					for(int j = 1; j < encabezado.getEncabezado().getTitulosTabla().size(); j++){
						cellTmp = new CellValueTableSEC(encabezado.getEncabezado().getEstilo(), encabezado.getEncabezado().getAlign(), encabezado.getEncabezado().getBorderStyle(), encabezado.getEncabezado().getVerticalAlign(), encabezado.getEncabezado().getRowSpan(), encabezado.getEncabezado().getTitulosTabla().get(j));
						lstTmp.add(crearCelda(cellTmp));
					}
				}else if (encabezado.getEncabezado().getTitulosTabla().size() <= encabezado.getEncabezado().getRowSpan()){
					PdfPCell celda = crearCelda(new CellValueTableSEC(encabezado.getEncabezado().getEstilo(), encabezado.getEncabezado().getAlign(), encabezado.getEncabezado().getBorderStyle(), encabezado.getEncabezado().getVerticalAlign(), encabezado.getEncabezado().getRowSpan(), encabezado.getEncabezado().getTitulosTabla().get(0)));
					tablaPdf.addCell(celda);
				}
			}
			for(int i=0; i < lstTmp.size(); i++){
				tablaPdf.addCell((PdfPCell) lstTmp.get(i));
			}
		
	}
	
	/**
	 * M�todo encargado de agregar una tabla al formulario
	 * @param tabla Objeto con los datos a agregar y las opciones de formato
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected void addTabla(TableSEC tabla) throws MalformedURLException, IOException, DocumentException{
		    documentPDF.add(crearTabla(tabla));
	}
	
	
	/**
	 * M�todo encargado de agregar una tabla al formulario
	 * @param tabla Objeto con los datos a agregar y las opciones de formato
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected PdfPTable crearTabla(TableSEC tabla) throws MalformedURLException, IOException, DocumentException{
		
		PdfPTable tablaTmp = new PdfPTable(tabla.getAnchoColumnas().length);
		tablaTmp.setWidths(tabla.getAnchoColumnas());
		tablaTmp.setWidthPercentage(tabla.getAnchoTotal());
		//ENCABEZADOS
		if(tabla.getEncabezados() != null){
			generarEncabezadosTabla(tabla.getEncabezados(), tablaTmp);	
		}
		
		//FIN ENCABEZADOS
		//FILAS DE DATOS
		for(int i=0; i < tabla.getFilas().size(); i++){
			RowTableSEC row = (RowTableSEC) tabla.getFilas().get(i);
			for(int j=0; j < row.getLstCols().size(); j++){
				tablaTmp.addCell(this.addCell((CellValueTableSEC) row.getLstCols().get(j)));
			}
		}
		//FIN FILAS DE DATOS
		return tablaTmp;
	}
			
	/**
	 * M�todo encargado de agregar una tabla al formulario a partir de una fila
	 * @param rows fila con las celdas a agregar
	 * @param cols cantidad de columnas de la tabla
	 * @param tabla Tabla con los datos de formato
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected void addTabla(List<RowTableSEC> rows , int cols, TableSEC tabla) throws MalformedURLException, IOException, DocumentException{
		
			PdfPTable tablaSeccionIII = new PdfPTable(cols);
			tablaSeccionIII.setWidths(tabla.getAnchoColumnas());
			tablaSeccionIII.setWidthPercentage(tabla.getAnchoTotal());
			for(int i=0; i < rows.size(); i++){
				RowTableSEC row = (RowTableSEC) rows.get(i);
				for(int j=0; j < row.getLstCols().size(); j++){
					tablaSeccionIII.addCell(this.addCell((CellValueSEC) row.getLstCols().get(j)));
				}
			}
		    documentPDF.add(tablaSeccionIII);
	}
	
	/**
	 * M�todo encargado de generar una secci�n a partir de una lista de tablas
	 * @param tablas Una lista de tipo List con las tablas de tipo TableSEC que se deben agregar a las secci�n
	 * @param anchos Ancho que debe contener cada parte de la secci�n a generar, es decir, 
	 * el espacio que ocupar� cada tabla 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected void addTabla(List<TableSEC> tablas, float[] anchos) throws MalformedURLException, IOException, DocumentException{
			PdfPTable outerTable = new PdfPTable(tablas.size());
			outerTable.setWidthPercentage(100);
			outerTable.setWidths(anchos);
			PdfPCell celda = null;
			for(int i=0; i < tablas.size(); i++){
				celda = new PdfPCell(crearTabla((TableSEC) tablas.get(i)));
				celda.setBorder(BorderStyleSEC.NO_BORDER);
				outerTable.addCell(celda);
			}
		    documentPDF.add(outerTable);
	}
	
	/**
	 * M�todo encargado de generar una secci�n a partir de una lista de tablas
	 * @param tablas Una lista de tipo List con las tablas de tipo TableSEC que se deben agregar a las secci�n
	 * @param anchos Ancho que debe contener cada parte de la secci�n a generar, es decir, 
	 * el espacio que ocupar� cada tabla 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected void addTablaF4(List<TableSEC> tablas, float[] anchos, String textoRecuadro) throws MalformedURLException, IOException, DocumentException{
		PdfPTable outerTable = new PdfPTable(tablas.size());
		outerTable.setWidthPercentage(100);
		outerTable.setWidths(anchos);
		PdfPCell celda = new PdfPCell(RecuadroUtil.generarRecuadroF4(new int[]{60}, textoRecuadro));
		outerTable.addCell(celda);
		for(int i=0; i < tablas.size(); i++){
			if(i != 0){
				celda = new PdfPCell(crearTabla((TableSEC) tablas.get(i)));
				celda.setBorder(BorderStyleSEC.NO_BORDER);
				outerTable.addCell(celda);					
			}
		}
	    documentPDF.add(outerTable);
	}
			
	/**
	 * M�todo encargado de crear una celda PDF
	 * @param cellSec objeto con los formatos de la celda a crear
	 * @return Celda PDF
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws BadElementException 
	 */
	protected PdfPCell addCell(CellValueSEC cellSec) throws BadElementException, MalformedURLException, IOException{
		PdfPCell celda = crearCelda(cellSec);
		return celda;
	}
			
	/**
	 * M�todo encargado de crear el encabezado de un formulario a partir de una lista de tablas
	 * contenidas en el atributo lstSecciones de la clase EncabezadoSEC
	 * @param encabezado objeto con las secciones del encabezado
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected void crearEncabezadoPorSecciones(EncabezadoSEC encabezado) throws MalformedURLException, IOException, DocumentException{
		  PdfPTable outertable = new PdfPTable(encabezado.getLstSecciones().size());
		  outertable.setWidthPercentage(100);
	      outertable.setWidths(new int[]{10,75,35});
		  
          PdfPCell seccion = null;
          TableSEC tabla = new TableSEC();
		  for(int i=0; i < encabezado.getLstSecciones().size(); i++){
			  tabla = (TableSEC) encabezado.getLstSecciones().get(i);
			  if(tabla.getRadio() > 1){
				  seccion = RecuadroUtil.generarRecuadro(crearTabla(tabla), 4, tabla.getRadio());
			  }else{
				  seccion = new PdfPCell(crearTabla(tabla));
				  seccion.setBorder(PdfPCell.NO_BORDER);
				  seccion.setPaddingTop(6);
				    
			  }
			  outertable.addCell(seccion);
		  }
	       documentPDF.add(outertable);	  
	}
	
	/**
	 * M�todo encargado de crear el encabezado de un formulario a partir de una lista de tablas
	 * contenidas en el atributo lstSecciones de la clase EncabezadoSEC
	 * @param encabezado objeto con las secciones del encabezado
	 * @param writer 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected void crearEncabezadoPorSeccionesDinamico(EncabezadoSEC encabezado, PdfWriter writer) throws MalformedURLException, IOException, DocumentException{
		PdfPTable outertable = new PdfPTable(encabezado.getLstSecciones().size());
		outertable.setWidthPercentage(100);
		outertable.setWidths(new int[]{8,72,40});
		
		PdfPCell seccion = null;
		TableSEC tabla = new TableSEC();
		for(int i=0; i < encabezado.getLstSecciones().size(); i++){
			tabla = (TableSEC) encabezado.getLstSecciones().get(i);
			if(tabla.getRadio() > 1){
				seccion = RecuadroUtil.generarRecuadro(crearTabla(tabla), 4, tabla.getRadio());
			}else{
				seccion = new PdfPCell(crearTabla(tabla));
				seccion.setBorder(PdfPCell.NO_BORDER);
				seccion.setPaddingTop(6);
				
			}
			outertable.addCell(seccion);
		}
		outertable.setTotalWidth(527);
        outertable.setLockedWidth(false);
        outertable.getDefaultCell().setFixedHeight(20);
		outertable.writeSelectedRows(0, -1, 30, 830, writer.getDirectContent());	  
	}
	
	
	/**
	 * Permite agregar filas a una tabla de datos, especificando la cantidad de columnas
	 * y filas a agregar
	 * @param cantidadFilas cantidad de filas a agregar
	 * @param cantidadColumnas cantidad de columnas de la tabla
	 * @param celdaMuestra contiene el formato de las celdas y en caso de ser datos homogeneos 
	 * se pueden agregar datos en el atributo value del objeto
	 * @return lista de filas creadas
	 */
	protected List<RowTableSEC> addFilas(int cantidadFilas, int cantidadColumnas, CellValueTableSEC celdaMuestra){
		List<RowTableSEC> filas = new ArrayList<RowTableSEC>();
		RowTableSEC fila = null;
		for(int i=0; i < cantidadFilas; i++){
			List<CellValueTableSEC> lstColumnas = new ArrayList<CellValueTableSEC>();
			fila = new RowTableSEC();
			for(int j=0; j < cantidadColumnas; j++){
				lstColumnas.add(celdaMuestra);
				fila.setLstCols(lstColumnas);
			}
			filas.add(fila);
		}
		
		return filas;
	}
	
	
	/**
	 * M�todo para controlar lo que se desea visualizar en el PDF
	 * cuando un valor es NULO dependiendo de su tipado
	 * @param value Objeto a verificar su nulidad
	 * @return Representaci�n del NULO en el formato PDF
	 */
	public String getValueNotNull(String value) {
		
		if ( value == null ) {
			return new String("");
		}
		
		return value;
		
	}
	
	/**
	 * M�todo encargado crear una celda para visualizar en el reporte PDF
	 * @param lstCeldas: lista de las celdas agregadas
	 * @param label: Celda con la etiqueta a mostrar
	 * @param value: Valor de la etiqueta a mostrar
	 * @return lstCeldas con la celda agregada
	 */
	protected List<CellSEC> addCeldas(List<CellSEC> lstCeldas, CellValueSEC label, CellValueSEC value){
		CellSEC celda = new CellSEC();		
		celda.setLabel(label);
		celda.setValue(value);
		lstCeldas.add(celda);	
		
		return lstCeldas;
	}
	
	/**
	 * M�todo encargado de generar una secci�n a partir de una lista de tablas
	 * @param tablas Una lista de tipo List con las tablas de tipo TableSEC que se deben agregar a las secci�n
	 * @param anchos Ancho que debe contener cada parte de la secci�n a generar, es decir, 
	 * el espacio que ocupar� cada tabla 
	 * @param alto Determina el alto de las filas de la tabla
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected void addTabla(List<TableSEC> tablas, float[] anchos, float alto) throws MalformedURLException, IOException, DocumentException{
		PdfPTable outerTable = new PdfPTable(tablas.size());
		outerTable.setWidthPercentage(100);
		outerTable.setWidths(anchos);
		PdfPCell celda = null;
		for(int i=0; i < tablas.size(); i++){
			celda = new PdfPCell(crearTabla((TableSEC) tablas.get(i), alto));
			celda.setBorder(BorderStyleSEC.NO_BORDER);						
			outerTable.addCell(celda);
		}
	    documentPDF.add(outerTable);
	}
	
	/**
	 * M�todo encargado de crear una celda IText
	 * @param cellValueSec objeto con los datos de formato de la celda a crear
	 * @param alto Determina el alto de las filas de la tabla
	 * @return Objeto con el formato y los datos recibidos en el par�metro de entrada
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws BadElementException 
	 */
	private PdfPCell crearCelda(CellValueSEC cellValueSec, float alto) throws BadElementException, MalformedURLException, IOException {
		PdfPCell pdfCell = null;
		if(!cellValueSec.isImagen()){
			Paragraph phrase = new Paragraph((cellValueSec.getValue()==null || cellValueSec.getValue().toString().equals(""))?" ":cellValueSec.getValue().toString(), cellValueSec.getEstilo());
			pdfCell = new PdfPCell(phrase);
			pdfCell.setMinimumHeight(alto);
		}else{
			Image foto = Image.getInstance(cellValueSec.getValue().toString());				
		    foto.scaleToFit(cellValueSec.getFit()[0], cellValueSec.getFit()[1]);
		    pdfCell = new PdfPCell(foto);
		}
		
		pdfCell.setHorizontalAlignment(cellValueSec.getAlign());
		pdfCell.setBorder(cellValueSec.getBorderStyle());
		pdfCell.setRowspan(cellValueSec.getRowSpan());
		pdfCell.setVerticalAlignment(cellValueSec.getVerticalAlign());
		pdfCell.setPadding(cellValueSec.getPadding());
		pdfCell.setColspan(cellValueSec.getColSpan());
		return pdfCell;
	}
			
	/**
	 * M�todo encargado de agregar una tabla al formulario
	 * @param tabla Objeto con los datos a agregar y las opciones de formato	 
	 * @param alto define el alto de la fila, para personalizar cada tabla
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws DocumentException 
	 */
	protected PdfPTable crearTabla(TableSEC tabla, float alto) throws MalformedURLException, IOException, DocumentException{		
		PdfPTable tablaTmp = new PdfPTable(tabla.getAnchoColumnas().length);
		tablaTmp.setWidths(tabla.getAnchoColumnas());
		tablaTmp.setWidthPercentage(tabla.getAnchoTotal());			
		if(tabla.getEncabezados() != null){
			generarEncabezadosTabla(tabla.getEncabezados(), tablaTmp);	
		}
		for(int i=0; i < tabla.getFilas().size(); i++){
			RowTableSEC row = (RowTableSEC) tabla.getFilas().get(i);				
			for(int j=0; j < row.getLstCols().size(); j++){
				tablaTmp.addCell(this.addCell((CellValueTableSEC) row.getLstCols().get(j), alto));
			}
		}
		return tablaTmp;
	}
	
	/**
	 * M�todo encargado de crear una celda PDF
	 * @param cellSec objeto con los formatos de la celda a crear
	 * @param alto define el alto de la fila de la tabla
	 * @return Celda PDF
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws BadElementException 
	 */
	protected PdfPCell addCell(CellValueSEC cellSec, float alto) throws BadElementException, MalformedURLException, IOException{
		PdfPCell celda = crearCelda(cellSec, alto);
		return celda;
	}
	
	/**
	 * Metodo encargado de cortar una palabra si excede el tamanno que se le envia como parametro.
	 * @param palabra String de entrada con la palabra a recortar de ser necesario.
	 * @param tamano Int con la longitud maxima de la palabra.
	 * @return String con la palabra recortada si excede el tamanno.
	 */
	protected String cortarPalabra(String palabra, int tamano){
		if(palabra != null){
			return (palabra.length() > tamano ? palabra.substring(0, tamano) : palabra);			
		}
		return " ";
	}
	
	/**
	 * M�todo encargado de agregar una lista de celdas
	 * @param lstTitulos Lista de los valores que van dentro de las celdas
	 * @param estilo Estilo de la letra
	 * @param lstBorde lista que contiene el estilo para cada borde de la celda
	 * @return Lista de CellSEC creadas
	 */
	protected ArrayList<CellSEC> agregarValores(List<String> lstTitulos, Font estilo,  List<Integer> lstBorde){
		ArrayList<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		CellSEC cellTmp;
		CellValueSEC valueTmp;
		for(int i=0; i < lstTitulos.size(); i++){
			cellTmp = new CellSEC();
			valueTmp = new CellValueSEC(estilo, Integer.parseInt(lstBorde.get(i).toString()), lstTitulos.get(i));
			cellTmp.setValue(valueTmp);
			lstCeldas.add(cellTmp);
		}
		return lstCeldas;
	}
	
	/**
	 * M�todo encargado de agregar una lista de celdas
	 * @param lstTitulos Lista de los valores que van dentro de las celdas
	 * @param estilo Estilo de la letra
	 * @param lstBorde lista que contiene el estilo para cada borde de la celda
	 * @param lstAlign Lista con las alineaciones de el PDF
	 * @return Lista de CellSEC creadas
	 */
	protected ArrayList<CellSEC> agregarValores(List<String> lstTitulos, Font estilo, List<Integer> lstBorde, List<String> lstAlign){
		ArrayList<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		CellSEC cellTmp;
		CellValueSEC valueTmp;
		for(int i=0; i < lstTitulos.size(); i++){
			cellTmp = new CellSEC();
			valueTmp = new CellValueSEC(estilo, Integer.parseInt(lstAlign.get(i).toString()), 
										Integer.parseInt(lstBorde.get(i).toString()), lstTitulos.get(i));
			cellTmp.setValue(valueTmp);
			lstCeldas.add(cellTmp);
		}
		return lstCeldas;
	}
	
	/**
	 * M�todo encargado de crear el encabezado del documento dependiendo del tipo de 
	 * usuario y el tipo de operaci�n para el formulario 10
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param writer 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	protected void crearEncabezadoF11(EncabezadoSEC encabezado, PdfWriter writer) throws DocumentException, MalformedURLException, IOException {
		boolean esInterno = false;
		if(encabezado.getTipoUsuario() == 1){
			esInterno = true;
		}
		PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,63,30});
        outertable.setTotalWidth(527);
        outertable.setLockedWidth(false);
        outertable.getDefaultCell().setFixedHeight(20);
        
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario(): "", FontStyleSEC.HELVETICA_BOLD_8PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulos(encabezado));
		
		RoundBR round = new RoundBR();
		round.setRadius(8);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable innertable = new PdfPTable(4);
		innertable.setWidths(new int[]{10,12,10,12});

		Phrase phrase = new Phrase(USO_EXCLUSIVO_BR, esInterno ? FontStyleSEC.HELVETICA_7PTS : FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
		cell.setVerticalAlignment(Chunk.ALIGN_TOP);
		innertable.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		innertable.addCell(cell);

		phrase = new Phrase(esInterno ? "Fecha Radicaci�n" : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getFechaRadicacion() != null && esInterno) ? encabezado.getRecuadro().getFechaRadicacion() : " ");
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setColspan(1);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);
		
		phrase = new Phrase(esInterno ? "N�mero Radicaci�n" : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setColspan(1);
		cell.setBorder(Rectangle.NO_BORDER);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getNumeroRadicacion() != null && esInterno) ? encabezado.getRecuadro().getNumeroRadicacion() : " ");
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);				

		cell = new PdfPCell(innertable);
		cell.setPadding(4);
		cell.setCellEvent(roundRectangle);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);

		outertable.addCell(cell);
		outertable.writeSelectedRows(0, -1, 34, 823, writer.getDirectContent());
	}
	
	/**
	 * M�todo para obtener la ruta del logo del banco de la rep�blica.
	 * @return String Ruta del Logo
	 */
	protected String getPathLogo() {
		
		if (pathLogo == null) { 
			//pathLogo = UtilidadSec.cargarParametroSistema(EParametrosSistema.PARAM_PATH_LOGO); 
		}
		
		return pathLogo;
	}
	
	/**
	 * M�todo para obtener la imagen del Logo del BR
	 * @return Image Objeto con la imagen.
	 * @throws IOException 
	 * @throws MalformedURLException 
	 * @throws BadElementException 
	 */
	protected Image getLogoBR() throws BadElementException, MalformedURLException, IOException {
		if (logoBR == null) {
			logoBR = Image.getInstance(getPathLogo());
		}
		return logoBR;
	}
	
	/**
	 * M�todo para obtener la ruta donde se genera el PDF
	 * @return
	 */
	public String getPathPDF() {
		return pathPDF;
	}

	/**
	 * M�todo para generar los PDF en la ruta pasada como parametro
	 * @param pathPDF String Ruta pasada como parametro.
	 */
	public void setPathPDF(String pathPDF) {
		this.pathPDF = pathPDF;
	}
	
	/**
	 * Metodo encargado de crear el pie de pagina del formulario 10.
	 * @param writer Instancia del pdfWriter para el manejo de evento.
	 * @param total Numero de paginas total.
	 * @param label Titulo del campo.
	 * @param value Valor del campo.
	 * @throws DocumentException 
	 */
	protected void crearPiePaginaAltura(PdfWriter writer, PdfTemplate total, String label, String value, float altura) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidths(new int[] { 98, 2 });
		table.setTotalWidth(523);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(-10);
		
		Phrase phrase = new Phrase(label, FontStyleSEC.HELVETICA_7PTS);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		phrase = new Phrase(value != "" ? value : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setFixedHeight(altura);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(Rectangle.BOX);
		table.addCell(cell);
		
		phrase = new Phrase(String.format("P�gina %3d de", writer.getPageNumber()), FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		Image imagen = Image.getInstance(total);
		imagen.scalePercent(52F);
		cell = new PdfPCell(imagen);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		table.writeSelectedRows(0, -1, 36, 90, writer.getDirectContent());
	}
	
	/**
	 * Metodo encargado de crear el pie de pagina del formulario 10.
	 * @param writer Instancia del pdfWriter para el manejo de evento.
	 * @param total Numero de paginas total.
	 * @param label Titulo del campo.
	 * @param value Valor del campo.
	 * @throws DocumentException 
	 */
	protected void crearPiePagina(PdfWriter writer, PdfTemplate total, String label, String value) throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setWidths(new int[] { 98, 2 });
		table.setTotalWidth(523);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(-10);
		
		Phrase phrase = new Phrase(label, FontStyleSEC.HELVETICA_7PTS);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		phrase = new Phrase(value != "" ? value : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(Rectangle.BOX);
		table.addCell(cell);
		
		phrase = new Phrase(String.format("P�gina %3d de", writer.getPageNumber()), FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		Image imagen = Image.getInstance(total);
		imagen.scalePercent(52F);
		cell = new PdfPCell(imagen);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		table.writeSelectedRows(0, -1, 36, 90, writer.getDirectContent());
	}
	
	/**
	 * M�todo encargado de crear el encabezado del documento dependiendo del tipo de 
	 * usuario y el tipo de operaci�n para el formulario 8
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param writer 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	protected void crearEncabezadoF8(EncabezadoSEC encabezado, PdfWriter writer) throws DocumentException, MalformedURLException, IOException {
		boolean esInterno = false;
		if(encabezado.getTipoUsuario() == 1){
			esInterno = true;
		}
		PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,60,33});
        outertable.setTotalWidth(527);
        outertable.setLockedWidth(false);
        outertable.getDefaultCell().setFixedHeight(20);
        
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario():"", FontStyleSEC.HELVETICA_7PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulos(encabezado));
		

		PdfPTable innertable = new PdfPTable(2);
		innertable.setWidths(new int[]{11,11});

		Phrase phrase = new Phrase("I.TIPO DE OPERACI�N", esInterno ? FontStyleSEC.HELVETICA_BOLD_8PTS : FontStyleSEC.HELVETICA_BOLD_8PTS);
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setVerticalAlignment(Chunk.ALIGN_TOP);
		innertable.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		innertable.addCell(cell);

		phrase = new Phrase(esInterno ? "1. N�mero" : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setFixedHeight(13f);
		cell.setBorder(Rectangle.BOX);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getNumeroRadicacion() != null && esInterno) ? encabezado.getRecuadro().getNumeroRadicacion() : " ");
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setFixedHeight(13f);
		cell.setColspan(1);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);
		
		phrase = new Phrase(esInterno ? "2. Fecha AAAA-MM-DD" : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setFixedHeight(13f);
		cell.setColspan(1);
		cell.setBorder(Rectangle.BOX);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getFechaRadicacion() != null && esInterno) ? encabezado.getRecuadro().getFechaRadicacion() : " ");
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setFixedHeight(13f);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);				

		cell = new PdfPCell(innertable);
		cell.setPadding(4);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);

		outertable.addCell(cell);
		outertable.writeSelectedRows(0, -1, 34, 823, writer.getDirectContent());
	}
	
	protected OutputStream createDocument() throws IOException {
		OutputStream outPut = null;
		
		File tmpFile = new File(getPathPDF());
		
		if (! tmpFile.getParentFile().exists()) {
			 tmpFile.getParentFile().mkdirs();
		}
		
		tmpFile.getParentFile().setWritable(true);
		tmpFile.setWritable(true); 
		tmpFile.createNewFile();
		outPut = new FileOutputStream(tmpFile);
		return outPut;
	}

	

	
	/**
	 * M�todo encargado de crear el encabezado del documento dependiendo del tipo de 
	 * usuario y el tipo de operaci�n para el formulario 11
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	protected void crearEncabezadoF11(EncabezadoSEC encabezado) throws DocumentException, MalformedURLException, IOException {
		boolean esInterno = false;
		if(encabezado.getTipoUsuario() == 1){
			esInterno = true;
		}
		PdfPTable outertable = new PdfPTable(3);
        outertable.setWidthPercentage(100);
        outertable.setWidths(new int[]{7,63,30});
        outertable.setTotalWidth(527);
        outertable.setLockedWidth(false);
        outertable.getDefaultCell().setFixedHeight(20);
        
        
        PdfPCell cell = new PdfPCell(new Phrase(encabezado.getTituloFormulario()!=null?encabezado.getTituloFormulario():"", FontStyleSEC.HELVETICA_BOLD_8PTS));
        cell.setColspan(3);
        cell.setHorizontalAlignment(Chunk.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        outertable.addCell(cell);
        outertable.addCell(generarImagen(encabezado));
        outertable.addCell(generarTitulos(encabezado));
		
		RoundBR round = new RoundBR();
		round.setRadius(8);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable innertable = new PdfPTable(2);
		innertable.setWidths(new int[]{10,12});

		Phrase phrase = new Phrase(USO_EXCLUSIVO_BR, esInterno ? FontStyleSEC.HELVETICA_7PTS : FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
		cell.setVerticalAlignment(Chunk.ALIGN_TOP);
		innertable.addCell(cell);

		phrase = new Phrase();
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		innertable.addCell(cell);

		phrase = new Phrase(esInterno ? "Fecha Radicaci�n" : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setBorder(Rectangle.NO_BORDER);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getFechaRadicacion() != null && esInterno) ? encabezado.getRecuadro().getFechaRadicacion() : " ");
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setColspan(1);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);
		
		phrase = new Phrase(esInterno ? "N�mero Radicaci�n" : " ", FontStyleSEC.HELVETICA_7PTS);
		cell = new PdfPCell(phrase);
		cell.setColspan(1);
		cell.setBorder(Rectangle.NO_BORDER);
		innertable.addCell(cell);
		phrase = new Phrase((encabezado.getRecuadro().getNumeroRadicacion() != null && esInterno) ? encabezado.getRecuadro().getNumeroRadicacion() : " ");
		phrase.getFont().setSize(6F);
		cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
		cell.setBorder(esInterno ? Rectangle.BOX : Rectangle.NO_BORDER);
		innertable.addCell(cell);				

		cell = new PdfPCell(innertable);
		cell.setPadding(4);
		cell.setCellEvent(roundRectangle);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);

		outertable.addCell(cell);
		documentPDF.add(outertable);
	}
	
	/**
	 * M�todo encargado de generar el encabezado para el usuario interno
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado 
	 * @param writer 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void encabezadoUsuarioInternoDinamico(EncabezadoSEC encabezado, boolean cuadroExtendido, boolean recuadroSoloOperacion, PdfWriter writer) throws DocumentException, MalformedURLException, IOException{
		LOG.debug("[ReportesSEC][encabezadoUsuarioInternoDinamico][INICIO]");
		PdfPTable outertable = generarImagenTitulos(encabezado);
		if(encabezado.getTipoOperacion() == 0){//USUARIO INTERNO - SIN OPERACION
			outertable.addCell(RecuadroUtil.generarRecuadroInternoSinOperacion(encabezado, 2, new int[]{17,20}, USO_EXCLUSIVO_BR));
		} else if(encabezado.getTipoOperacion() == 1 || encabezado.getTipoOperacion() == 2 || encabezado.getTipoOperacion() == 3 || encabezado.getTipoOperacion() == 4){ //USUARIO INTERNO - OPERACION MODICAR O ANULAR
			if(cuadroExtendido){
				outertable.addCell(RecuadroUtil.generarRecuadroInternoConOperacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));			        	
			}else if (recuadroSoloOperacion){
				outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{25}, USO_EXCLUSIVO_BR));
			}else{
				outertable.addCell(RecuadroUtil.generarRecuadroInternoModificacionAnulacion(encabezado, 3, new int[]{23,15,15}, USO_EXCLUSIVO_BR));
			}
		}
        outertable.setTotalWidth(527);
        outertable.setLockedWidth(false);
        outertable.getDefaultCell().setFixedHeight(20);
		outertable.writeSelectedRows(0, -1, 34, 823, writer.getDirectContent());	  
		LOG.debug("[ReportesSEC][encabezadoUsuarioInternoDinamico][FIN]");
	}
	
	/**
	 * M�todo encargado de generar el encabezado para el usuario externo
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado
	 * @param writer 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void encabezadoUsuarioExternoDinamico(EncabezadoSEC encabezado, boolean cuadroExtendido, PdfWriter writer , String title) throws MalformedURLException, IOException, DocumentException{
		LOG.debug("[ReportesSEC][encabezadoUsuarioExternoDinamico][INICIO]");
		PdfPTable outertable = generarImagenTitulos(encabezado);
		if(encabezado.getTipoOperacion() == 1){ //USUARIO EXTERNO - OPERACION INICIAL	        		 
			outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, title));
		}else if(encabezado.getTipoOperacion() == 2){ //USUARIO EXTERNO - MODIFICACION
			outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, title));	
		}else if(encabezado.getTipoOperacion() == 0){ //SOLO IMPRESION SIN SELECCIONAR TIPO OPERACION
			outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, title));	
		}else if(encabezado.getTipoOperacion() == 4){ //MODIFICACION (PARA EL F3)
			outertable.addCell(RecuadroUtil.generarRecuadroExtendido(encabezado, new int[]{53}, title));	
		}
        outertable.setTotalWidth(527);
        outertable.setLockedWidth(false);
        outertable.getDefaultCell().setFixedHeight(20);
		outertable.writeSelectedRows(0, -1, 34, 823, writer.getDirectContent());	
		LOG.debug("[ReportesSEC][encabezadoUsuarioExternoDinamico][FIN]");
	}
	
	/**
	 * M�todo encargado de generar el encabezado para el usuario sin autenticar
	 * @param encabezado Objeto con los datos necesarios para generar el encabezado
	 * @param writer 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void encabezadoUsuarioSinAutenticacionDinamico(EncabezadoSEC encabezado, PdfWriter writer) throws MalformedURLException, IOException, DocumentException{
		LOG.debug("[ReportesSEC][encabezadoUsuarioExternoDinamico][INICIO]");
		PdfPTable outertable = generarImagenTitulos(encabezado);
		outertable.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));
		outertable.setTotalWidth(527);
		outertable.setLockedWidth(false);
		outertable.getDefaultCell().setFixedHeight(20);
		outertable.writeSelectedRows(0, -1, 34, 823, writer.getDirectContent());	
		LOG.debug("[ReportesSEC][encabezadoUsuarioExternoDinamico][FIN]");
	}
	
	
	// GETTER & SETTER
	
	public float getAlturaCelda() {
		return alturaCelda;
	}

	public void setAlturaCelda(float alturaCelda) {
		this.alturaCelda = alturaCelda;
	}
	
	
	/**
	 * 
	 * @param writer
	 * @param tipoOperacion
	 * @param numeroRadicacion
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	protected void crearSeccionIF11(PdfWriter writer, String tipoOperacion, String numeroRadicacion, boolean tipoUsuarioNoLog) throws DocumentException, MalformedURLException, IOException {
		
		PdfPTable table = new PdfPTable(4);
		table.setWidths(new int[] { 65, 25, 8, 2 });
		table.setTotalWidth(523);
		table.setLockedWidth(false);
		table.getDefaultCell().setFixedHeight(20);
		
		List<String> lstElementos = Arrays.asList(new String[]{"", "","",""});
		ArrayList<CellSEC> lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_BOLD_8PTS, BorderStyleSEC.NO_BORDER);
		
		PdfPCell cellTmp;
		for(int i=0; i < lstCeldas.size(); i++){
			cellTmp = new PdfPCell(this.addFila((CellSEC)lstCeldas.get(i), Constantes.CIEN));
			cellTmp.setBorder(BorderStyleSEC.NO_BORDER);
			table.addCell(cellTmp);
		}
		
		lstElementos = Arrays.asList(new String[]{"", "I.TIPO DE OPERACI�N","",""});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_BOLD_8PTS, BorderStyleSEC.NO_BORDER);
		
		for(int i=0; i < lstCeldas.size(); i++){
			cellTmp = new PdfPCell(this.addFila((CellSEC)lstCeldas.get(i), Constantes.CIEN));
			cellTmp.setBorder(BorderStyleSEC.NO_BORDER);
			table.addCell(cellTmp);
		}

		lstElementos = Arrays.asList(new String[]{"", "1. Inversi�n extranjera en Colombia", "", ((tipoOperacion != null && !numeroRadicacion.isEmpty()) || tipoUsuarioNoLog) ? tipoOperacion.equals("E")? "X" : "" : ""});
		List<Integer> lstBordes = Arrays.asList(new Integer[] { BorderStyleSEC.NO_BORDER, BorderStyleSEC.NO_BORDER, BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER});
		List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.CENTER)});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes, lstAling);
		
		for(int i=0; i < lstCeldas.size(); i++){
			cellTmp = new PdfPCell(this.addFila((CellSEC)lstCeldas.get(i), Constantes.CIEN));
			cellTmp.setBorder(BorderStyleSEC.NO_BORDER);
			table.addCell(cellTmp);
		}
		
		lstElementos = Arrays.asList(new String[]{"", "","",""});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_BOLD_8PTS, BorderStyleSEC.NO_BORDER);
		
		for(int i=0; i < lstCeldas.size(); i++){
			cellTmp = new PdfPCell(this.addFila((CellSEC)lstCeldas.get(i), Constantes.CIEN));
			cellTmp.setBorder(BorderStyleSEC.NO_BORDER);
			table.addCell(cellTmp);
		}
		
		lstElementos = Arrays.asList(new String[]{"", "2. Inversi�n Colombiana en el exterior", "", ((tipoOperacion != null && !numeroRadicacion.isEmpty())  || tipoUsuarioNoLog) ? tipoOperacion.equals("C")? "X" : "" : ""});
		lstBordes = Arrays.asList(new Integer[] { BorderStyleSEC.NO_BORDER, BorderStyleSEC.NO_BORDER, BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER});
		lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.CENTER)});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes, lstAling);

		for(int i=0; i < lstCeldas.size(); i++){
			cellTmp = new PdfPCell(this.addFila((CellSEC)lstCeldas.get(i), Constantes.CIEN));
			cellTmp.setBorder(BorderStyleSEC.NO_BORDER);
			table.addCell(cellTmp);
		}
		
		table.writeSelectedRows(0, -1, 36, 760, writer.getDirectContent());
	}
	
	protected void crearEncabezadoF3(EncabezadoSEC encabezado, PdfWriter writer) throws MalformedURLException, IOException, DocumentException {
		PdfPTable table = generarImagenTitulosF4(encabezado);
		table.setTotalWidth(527);
		table.setLockedWidth(false);
		table.getDefaultCell().setFixedHeight(20);
		table.addCell(RecuadroUtil.generarRecuadroBasico(new int[]{53}, USO_EXCLUSIVO_BR));
		table.writeSelectedRows(0, -1, 36, 800, writer.getDirectContent());
	}
	
	protected void addTablaEncabezadoDinamico(List<TableSEC> tablas, float[] anchos, PdfWriter writer, float altura) throws MalformedURLException, IOException, DocumentException{
		PdfPTable outerTable = new PdfPTable(tablas.size());
		outerTable.setTotalWidth(527);
		outerTable.setLockedWidth(false);
		outerTable.getDefaultCell().setFixedHeight(20);
		outerTable.setWidthPercentage(100);
		outerTable.setWidths(anchos);
		PdfPCell celda = null;
		for(int i=0; i < tablas.size(); i++){
			celda = new PdfPCell(crearTabla(tablas.get(i)));
			celda.setBorder(BorderStyleSEC.NO_BORDER);
			outerTable.addCell(celda);
		}
		outerTable.writeSelectedRows(0, -1, 32, 715, writer.getDirectContent());
	}

	public String getExcepcionGenerada() {
		return excepcionGenerada;
	}

	public void setExcepcionGenerada(String excepcionGenerada) {
		this.excepcionGenerada = excepcionGenerada;
	}
	
	/**
	 * M�todo encargado de agregar una lista de celdas
	 * @param lstTitulos Lista de los valores que van dentro de las celdas
	 * @param estilo Estilo de la letra
	 * @param lstBorde lista que contiene el estilo para cada borde de la celda
	 * @param lstAlign Lista con las alineaciones de el PDF
	 * @return Lista de CellSEC creadas
	 */
	protected ArrayList<CellSEC> agregarValoresNoWrap(List<String> lstTitulos, Font estilo, List<Integer> lstBorde, List<String> lstAlign){
		ArrayList<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		CellSEC cellTmp;
		CellValueSEC valueTmp;
		for(int i=0; i < lstTitulos.size(); i++){
			cellTmp = new CellSEC();
			valueTmp = new CellValueSEC(estilo, Integer.parseInt(lstAlign.get(i).toString()), 
										Integer.parseInt(lstBorde.get(i).toString()), lstTitulos.get(i));
			valueTmp.setNoWrap(true);
			cellTmp.setValue(valueTmp);
			lstCeldas.add(cellTmp);
		}
		return lstCeldas;
	}
}