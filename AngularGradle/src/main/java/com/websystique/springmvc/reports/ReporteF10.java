package com.websystique.springmvc.reports;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.websystique.springmvc.utils.AlignTextSEC;
import com.websystique.springmvc.utils.BorderStyleSEC;
import com.websystique.springmvc.utils.CellSEC;
import com.websystique.springmvc.utils.CellValueSEC;
import com.websystique.springmvc.utils.Constantes;
import com.websystique.springmvc.utils.ExcepcionDatos;
import com.websystique.springmvc.utils.ExcepcionRespuestaPdf;
import com.websystique.springmvc.utils.FontStyleSEC;


/**
 * @author Indra 
 * Clase para el manejo del reporte del formulario 10.
 */
public class ReporteF10 extends ReportesSEC {
	/** Constantes de la clase. */
	private final static Logger LOG = Logger.getLogger(ReporteF10.class);
	private static final String NUEVO_BIGD_CERO = "0";
	private static final String NUEVO_BIGD_PUNTO = "0.00";
	private static final String INGRESO = "I";
	private static final String CONFIRMACION = "S";
	private static final String NI = "NI";
	private static final String YYYY_MM = "yyyy-MM";
	private static final String GUION = "-";
	private static final String SELECCION = "X";
	private static final String ESPACIO = " ";
	private static final String CAUSA = " Causa: ";
	static SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_Hmmss");
	static String fecha = sdf.format(new Date());
	private static final float ANCHO_SECCIONES = 100;
	private static final int ESPACIO_ENTRE_SECCIONES = 8;
	private static String nombreArchivoPdf = "Formulario10_" + fecha + ".pdf";
	private static String VACIO = "";
	protected static String TIPO_USUARIO = "1";
	protected static final float ALTURA_CELDA_F10 = 11.2f;
	private static int CANTIDAD_CONSORCIO = 2;
	private static int CANTIDAD_A = 5;
	private static int CANTIDAD_A_II = 5;
	private static int CANTIDAD_B = 2;
	private static int CANTIDAD_B_II = 2;
	private static int CANTIDAD_D = 3;
	private static int CONTADOR_CONSORCIO = 0;
	private static int CONTADOR_A = 0;
	private static int CONTADOR_B = 0;
	private static int CONTADOR_D = 0;

	protected static String LB_TITULO_ENCABEZADO = "Registro, Informe de Movimientos y/o Cancelaci�n Cuenta de Compensaci�n";
	protected static String LB_FORMULARIO_DIEZ = "Formulario No. 10";
	private static final String LB_CIRCULAR_REGLAMENTARIA = "Circular Reglamentaria Externa DCIN-83 de ";
	// Seccion 1
	private static String TITULO_SECCION_I = "I. OPERACI�N";
	private static String LB_1_TIPO_OP = "1. Tipo de operaci�n";
	private static String LB_2_REGISTRO =    "2. Registro";
	private static String LB_2_INFORME_MOV = "    Informe de movimientos";
	private static String LB_2_CANC_REGISTRO = "    Cancelaci�n del registro";
	private static String LB_2_FECHA = "Fecha (AAAA-MM-DD)";
	private static String LB_2_PERIODO = "Periodo reportado (AAAA-MM)";
	private static String LB_3_CODIGO_ASIGNADO = "3. C�digo asignado por el Banco de la Rep�blica";
	// Seccion 2
	private static String TITULO_SECCION_II = "II. IDENTIFICACI�N DEL TITULAR DE LA CUENTA";
	private static String LB_4_TIPO = "4. Tipo";
	private static String LB_5_NO_IDENTIFICACION = "5. N�mero de identificaci�n";
	private static String LB_5_DV = "DV";
	private static String LB_6_NOMBRE_RAZON_SOCIAL = "6. Nombre o raz�n social";
	private static String LB_7_CODIGO_CIUDAD = "7. C�digo ciudad";
	private static String LB_8_DIRECCION = "8. Direcci�n";
	private static String LB_9_TELEFONO = "9. Tel�fono";
	private static String LB_10_CODIGO_CIIU = "10. C�digo CIIU";
	private static String LB_10_CONSORCIO = "CONSORCIO, UNI�N TEMPORAL, SOCIEDAD DE HECHO O CONSTITUYENTES FIDUCIARIOS";
	private static String LB_11_TIPO = "11. Tipo";
	private static String LB_12_NO_IDENTIFICACION = "12. N�mero de identificaci�n";
	private static String LB_12_DV = "DV";
	private static String LB_13_NOMBRE = "13. Nombre";
	// Seccion 3
	private static String TITULO_SECCION_III = "III. IDENTIFICACI�N DE LA CUENTA EN EL EXTERIOR";
	private static String LB_14_NOMBRE_BANCO = "14. Nombre del banco";
	private static String LB_15_CODIGO_PAIS = "15.C�digo pa�s";
	private static String LB_16_CODIGO_CIUDAD = "16. C�digo ciudad";
	private static String LB_17_NUMERO_CUENTA = "17. N�mero de la cuenta";
	private static String LB_18_CODIGO_MONEDA = "18. C�digo moneda";
	// Seccion 4
	private static String TITULO_SECCION_IV = "IV. INFORME DE MOVIMIENTOS CUENTA DE COMPENSACI�N";
	private static String LB_19_SIN_MOVIMIENTO = "19. Sin movimiento";
	private static String SUBTITULO_A = "A. NUMERALES CAMBIARIOS";
	private static String LB_20_NUMERAL_INGRESO = "20. Numeral ingreso";
	private static String LB_21_VALOR_INGRESO = "21. Valor ingreso";
	private static String LB_22_NUMERAL_EGRESO = "22. Numeral egreso";
	private static String LB_23_VALOR_EGRESO = "23. Valor egreso";
	private static String SUBTITULO_B = "B. NUMERALES CAMBIARIOS POR DEVOLUCIONES";
	private static String LB_24_NUMERAL_EGRESO = "24. Numeral egreso";
	private static String LB_25_VALOR_INGRESO = "25. Valor ingreso";
	private static String LB_26_NUMERAL_INGRESO = "26. Numeral ingreso";
	private static String LB_27_VALOR_EGRESO = "27. Valor egreso";
	private static String LB_28_SALDO_ANTERIOR = "28. Saldo Anterior";
	private static String LB_29_INGRESOS = "29. Ingresos del periodo (21+25)";
	private static String LB_30_EGRESOS = "30. Egresos del periodo (23+27)";
	private static String LB_31_NUEVO_SALDO = "31. Nuevo saldo";
	private static String SUBTITULO_C = "C. INVERSIONES FINANCIERAS";
	private static String LB_32_NUEVO_SALDO_INV_FINANCIERA = "32. Nuevo saldo inversiones financieras";
	private static String LB_33_OVERNIGHT = "33. Overnight pendientes de redenci�n a fin de mes";
	private static String SUBTITULO_D = "D. IDENTIFICACI�N DEL TITULAR (CONTRAPARTE) VENDEDOR O COMPRADOR O IMC - IDENTIFICACI�N DEL TITULAR CONTRAPARTE DE UNA OPERACI�N INTERNA";
	private static String LB_34_TIPO = "34. Tipo";
	private static String LB_35_NUMERO_IDENTIFICACION = "35. N�mero de identificaci�n";
	private static String LB_35_DV = "DV";
	private static String LB_36_CODIGO_BR = "36. C�digo asignado en el BR   ";
	private static String LB_37_NUMERAL = "37. Numeral";
	private static String LB_38_VALOR = "38. Valor";
	private static String LB_38_PARRAFO = "Para los fines previstos en el art�culo 83 de la Constituci�n Pol�tica de Colombia, declaro bajo la gravedad de juramento que los conceptos, cantidades y dem�s datos consignados en el presente formulario son correctos y la fiel expresi�n de la verdad.";
	// Seccion 5
	private static String TITULO_SECCION_V = "V. IDENTIFICACI�N DEL REPRESENTANTE LEGAL, APODERADO O TITULAR DE LA CUENTA ";
	private static String LB_39_TIPO = "39. Tipo";
	private static String LB_40_NUMERO_IDENTIFICACION = "40. N�mero de identificaci�n";
	private static String LB_41_NOMBRE = "41. Nombre";
	private static String LB_42_FIRMA = "42. Firma";
	private static String LB_43_DIRECCION_NOTIFICACION = "43. Direcci�n para notificaci�n";
	private static String LB_44_CODIGO_CIUDAD = "44. C�digo ciudad";
	private static String LB_45_TELEFONO = "45. Tel�fono";
	private static String LB_46_CORREO_ELECTRONICO = "46. Correo electr�nico";

	// Variables
	private static String VR_3_CODIGO_ASIGNADO = VACIO;
	private static String VR_42_FIRMA = VACIO;
	protected String fechaCircular;
	protected static String textoEncabezado = VACIO;
	//protected MovimientoCuenta movimientoCuenta;
	protected String nombreArchivo;
	protected static Date vrFechaEncabezado = new Date();
	protected static Integer vrNumeroENcabezado = 0;
	protected String margenIzquierda = VACIO;
	protected String margenDerecha = VACIO;
	protected String margenArriba = VACIO;
	protected String margenAbajo = VACIO;
	protected String fechaRadicacion = VACIO;
	protected String numeroRadicacion = VACIO;
	protected String radicacion = VACIO;
	//private Vector<DetalleMovimiento> egresos; 
	//private Vector<DetalleMovimiento> ingresos;
	//private Vector<DetalleDevolucion> ingresosDevolucion;
	//private Vector<DetalleDevolucion> egresosDevolucion;
	private boolean esInterno;

	/**
	 * Metodo encargado de generar el pdf
	 */
	public String generarFormularioPDF()  throws ExcepcionRespuestaPdf {
		LOG.debug("[ReporteF10][generarFormularioPDF][Inicio]");
		try {
			nombreArchivoPdf = getNombreArchivo();
			if(!nombreArchivoPdf.contains(Constantes.EXTENSION_PDF)){
				nombreArchivoPdf += Constantes.EXTENSION_PDF;
			}
			//vrFechaEncabezado = movimientoCuenta.getCuenta().getFechaRadicacion();
			//vrNumeroENcabezado = movimientoCuenta.getCuenta().getNumeroRadicacion();
			textoEncabezado = LB_CIRCULAR_REGLAMENTARIA + fechaCircular;
			
			documentPDF = new Document(PageSize.A4, Float.valueOf(margenIzquierda), Float.valueOf(margenDerecha), Float.valueOf(margenArriba), Float.valueOf(margenAbajo));
			PdfWriter writer = PdfWriter.getInstance(documentPDF, new FileOutputStream(getPathPDF() + nombreArchivoPdf));
			ReporteF10Eventos f10Eventos = new ReporteF10Eventos(this.fechaRadicacion, this.numeroRadicacion, this.radicacion, this.esInterno);
			f10Eventos.setFechaCircular(getFechaCircular());
			setAlturaCelda(ALTURA_CELDA_F10);
			writer.setPageEvent(f10Eventos);
			documentPDF.open();
			generarSeccionI();
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
			generarSeccionII();
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
			generarSeccionIII();
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
			generarSeccionIV();
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
			generarSeccionV();
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
			seccionesDinamicas();
			documentPDF.close();
			
			if (getExcepcionGenerada() != null || f10Eventos.getExcepcionGenerada() != null) {
				throw new ExcepcionDatos(MSJ_ERROR_REPORTE);
			}
			
		} catch (ExcepcionDatos | DocumentException | IOException | NullPointerException | ArrayIndexOutOfBoundsException e) {
			ExceptionUtils.getStackTrace(e);
			LOG.error("[ReporteF10][generarFormularioPDF] " + e.getMessage() + CAUSA + e.getCause());
			throw new ExcepcionRespuestaPdf(MSJ_ERROR_REPORTE);
		}
		LOG.debug("[ReporteF10][generarFormularioPDF][Fin]");
		return getPathPDF();
	}
	

	/** Constructor vacio de la clase ReporteF10.java */
	public ReporteF10() {
	}
	
	/**
	 * 
	 * @param margenIzquierda
	 * @param margeDerecha
	 * @param margenArriba
	 * @param margenAbajo
	 */
	public ReporteF10 (String margenIzquierda,String  margenDerecha,String  margenArriba,String  margenAbajo,
			String fechaRadicacion, String numeroRadicacion, String radicacion){
		super (margenIzquierda, margenDerecha, margenArriba, margenAbajo);
		this.margenIzquierda = margenIzquierda;
		this.margenDerecha = margenDerecha;
		this.margenArriba = margenArriba;
		this.margenAbajo = margenAbajo;
		this.fechaRadicacion = fechaRadicacion;
		this.numeroRadicacion = numeroRadicacion;
		this.radicacion = radicacion;
		CONTADOR_CONSORCIO = 0;
		CONTADOR_A = 0;
		CONTADOR_B = 0;
		CONTADOR_D = 0;
	}
	
	/**
	 * Metodo encargado de generar la seccion uno del formulario 10.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void generarSeccionI() throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][generarSeccionI][Inicio]");
		List<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		float[] anchos = new float[1];
		anchos[0]=100;
		CellSEC cellTitulo = new CellSEC();
		CellValueSEC titulo = new CellValueSEC(FontStyleSEC.HELVETICA_BOLD_8PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, TITULO_SECCION_I);
		cellTitulo.setValue(titulo);
		lstCeldas.add(cellTitulo);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		lstCeldas = new ArrayList<CellSEC>();
		anchos = new float[3];
		anchos[0]=16;
		anchos[1]=14;
		anchos[2]=70;
		String tipoOp = VACIO;
//		if(Constantes.UNO == movimientoCuenta.getCuenta().getTipoOperacion()){
//			tipoOp = Constantes.TIPO_OP_INICIAL;
//		} else if(Constantes.DOS == movimientoCuenta.getCuenta().getTipoOperacion()){
//			tipoOp = Constantes.TIPO_OP_MODIFICACION;
//		}
		List<String> lstElementos = Arrays.asList(new String[]{LB_1_TIPO_OP, cortarPalabra(tipoOp, 17), VACIO});
		List<Integer> lstBordes = Arrays.asList(new Integer[] { BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER });
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[5];
		anchos[0]=28;
		anchos[1]=2;
		anchos[2]=3;
		anchos[3]=10;
		anchos[4]=57;
//		lstElementos = Arrays.asList(new String[]{LB_2_REGISTRO, cortarPalabra(movimientoCuenta.isRegistro() ? SELECCION : VACIO, 2), VACIO, 
//				cortarPalabra(movimientoCuenta.isRegistro()? (movimientoCuenta.getCuenta().getFechaApertura() == null ? VACIO : UtilidadSec.formatoFecha(movimientoCuenta.getCuenta().getFechaApertura())) : VACIO , 12), LB_2_FECHA});
//		lstBordes = Arrays.asList(new Integer[] { BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER});
//		List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT),  String.valueOf(AlignTextSEC.LEFT),  String.valueOf(AlignTextSEC.LEFT),  String.valueOf(AlignTextSEC.CENTER),  String.valueOf(AlignTextSEC.LEFT)});
//		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes, lstAling);
//		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
//
//		lstElementos = Arrays.asList(new String[]{LB_2_INFORME_MOV, cortarPalabra(movimientoCuenta.isInforme() ? SELECCION : VACIO, 2), VACIO, 
//				cortarPalabra(movimientoCuenta.getPeriodo() != null ? UtilidadSec.formatoFecha(movimientoCuenta.getPeriodo(), YYYY_MM) : VACIO, 12), LB_2_PERIODO});
//		lstBordes = Arrays.asList(new Integer[] { BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER});
//		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes,lstAling);
//		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
//
//		lstElementos = Arrays.asList(new String[]{LB_2_CANC_REGISTRO, cortarPalabra(movimientoCuenta.isCancelacionCuenta() ? SELECCION : VACIO, 2), VACIO, 
//				cortarPalabra(movimientoCuenta.getCuenta().getFechaCancelacion() != null ? UtilidadSec.formatoFecha(movimientoCuenta.getCuenta().getFechaCancelacion()) : VACIO, 12), LB_2_FECHA});
//		lstBordes = Arrays.asList(new Integer[] { BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER});
//		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes, lstAling);
//		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[2];
		anchos[0]=30;
		anchos[1]=70;
		lstElementos = Arrays.asList(new String[]{LB_3_CODIGO_ASIGNADO, VACIO});
		lstBordes = Arrays.asList(new Integer[] {BorderStyleSEC.NO_BORDER, BorderStyleSEC.NO_BORDER});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		try {
//			VR_3_CODIGO_ASIGNADO = (movimientoCuenta.getCuenta().getNumInterno() > 0 ? UtilidadSec.formatearDecimales(movimientoCuenta.getCuenta().getNumInterno(), 3) : VACIO);
//		} catch (ExcepcionDatos e) {
//			VR_3_CODIGO_ASIGNADO = VACIO;
//		}
		lstElementos = Arrays.asList(new String[]{cortarPalabra(VR_3_CODIGO_ASIGNADO, 37), VACIO});
		lstBordes = Arrays.asList(new Integer[] {BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		LOG.debug("[ReporteF10][generarSeccionI][Fin]");
	}
	
	/**
	 * Metodo encargado de generar la seccion dos del formulario 10.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void generarSeccionII() throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][generarSeccionII][Inicio]");
		List<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		float[] anchos = new float[1];
		anchos[0]=100;
		CellSEC cellTitulo = new CellSEC();
		CellValueSEC titulo = new CellValueSEC(FontStyleSEC.HELVETICA_BOLD_8PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, TITULO_SECCION_II);
		cellTitulo.setValue(titulo);
		lstCeldas.add(cellTitulo);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[4];
		anchos[0]=10;
		anchos[1]=20;
		anchos[2]=3;
		anchos[3]=67;
		List<String> lstElementos = Arrays.asList(new String[]{LB_4_TIPO, LB_5_NO_IDENTIFICACION, LB_5_DV, LB_6_NOMBRE_RAZON_SOCIAL});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{cortarPalabra(movimientoCuenta.getCuenta().getTitular().getTipoIdent().equalsIgnoreCase(VACIO) ? VACIO : movimientoCuenta.getCuenta().getTitular().getTipoIdent(), 12), 
//				cortarPalabra(movimientoCuenta.getCuenta().getTitular().getTipoIdent().equalsIgnoreCase(NI) ? (movimientoCuenta.getCuenta().getTitular().getNumIdent().equalsIgnoreCase(VACIO) ? VACIO : movimientoCuenta.getCuenta().getTitular()
//						.getNumIdent().substring(0, movimientoCuenta.getCuenta().getTitular().getNumIdent().length() - 1)) : 
//							(movimientoCuenta.getCuenta().getTitular().getNumIdent().equalsIgnoreCase(VACIO) ? VACIO : movimientoCuenta.getCuenta().getTitular().getNumIdent()), 24), 
//				cortarPalabra(movimientoCuenta.getCuenta().getTitular().getTipoIdent().equalsIgnoreCase(NI) ? (movimientoCuenta.getCuenta().getTitular().getNumIdent().equalsIgnoreCase(VACIO) ? VACIO : movimientoCuenta.getCuenta().getTitular()
//						.getNumIdent().substring((movimientoCuenta.getCuenta().getTitular().getNumIdent().length() - 1))) : (movimientoCuenta.getCuenta().getTitular().getNumIdent().equalsIgnoreCase(VACIO) ? VACIO : VACIO), 3), 
//				cortarPalabra(movimientoCuenta.getCuenta().getTitular().getNombre().equalsIgnoreCase(VACIO) ? VACIO : movimientoCuenta.getCuenta().getTitular().getNombre(), 83)});
//		List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT),String.valueOf(AlignTextSEC.LEFT),String.valueOf(AlignTextSEC.LEFT)});
//		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
//		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[2];
		anchos[0]=33;
		anchos[1]=67;
		lstElementos = Arrays.asList(new String[]{LB_7_CODIGO_CIUDAD, LB_8_DIRECCION});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{cortarPalabra(movimientoCuenta.getCuenta().getTitular().getCiudad().getCodigo().equalsIgnoreCase(VACIO) ? VACIO 
//				: (movimientoCuenta.getCuenta().getTitular().getCiudad().getCodigo() + (movimientoCuenta.getCuenta().getTitular().getCiudad().getDescripcion() != null ? 
//						GUION + movimientoCuenta.getCuenta().getTitular().getCiudad().getDescripcion() : VACIO)), 41), 
//				cortarPalabra(movimientoCuenta.getCuenta().getTitular().getDireccion() != null && movimientoCuenta.getCuenta().getTitular().getDireccion().equalsIgnoreCase(VACIO) ? VACIO : 
//					movimientoCuenta.getCuenta().getTitular().getDireccion(), 83)});
//		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);

		lstElementos = Arrays.asList(new String[]{LB_9_TELEFONO, LB_10_CODIGO_CIIU});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{
//				// 9.
//				cortarPalabra(movimientoCuenta.getCuenta().getTitular().getTelefono() == null && 
//				movimientoCuenta.getCuenta().getTitular().getTelefono().equalsIgnoreCase(VACIO) ? VACIO : movimientoCuenta.getCuenta().getTitular().getTelefono(), 41), 
//				// 10.
//				cortarPalabra(movimientoCuenta.getCuenta().getTitular().getCiiu()==null ? VACIO : (movimientoCuenta.getCuenta().getTitular().getCiiu().getCodigo() + (movimientoCuenta.getCuenta().getTitular()
//						.getCiiu().getDescripcion() != null && !movimientoCuenta.getCuenta().getTitular().getCiiu().getDescripcion().trim().equalsIgnoreCase(VACIO) ? GUION + movimientoCuenta.getCuenta().getTitular().getCiiu().getDescripcion() : VACIO)), 83)});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		addFilaVacia(ESPACIO_ENTRE_SECCIONES);
		consorcioDinamico(2);
		LOG.debug("[ReporteF10][generarSeccionII][Fin]");
	}

	private void consorcioDinamico(int cantidad) throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][consorcioDinamico][Inicio]");
		float[] anchos = new float[1];
		anchos[0]=100;
		List<String> lstElementos = Arrays.asList(new String[]{LB_10_CONSORCIO});
		ArrayList<CellSEC> lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_BOLD_8PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[4];
		anchos[0]=10;
		anchos[1]=20;
		anchos[2]=3;
		anchos[3]=67;
		lstElementos = Arrays.asList(new String[]{LB_11_TIPO, LB_12_NO_IDENTIFICACION, LB_12_DV, LB_13_NOMBRE});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		if(cantidad == 0){
			while(CONTADOR_CONSORCIO < 2){
				lstElementos = Arrays.asList(new String[]{
						cortarPalabra(ESPACIO, 35), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 27)});
				List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT)});
				lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
				CONTADOR_CONSORCIO++;
			}
		} else{
			while(CONTADOR_CONSORCIO < cantidad){
//				TitularSecundario titularSecundario = new TitularSecundario();
//				titularSecundario = (movimientoCuenta.getCuenta().getTitularSecundario().isEmpty() ? new TitularSecundario() : 
//					(TitularSecundario) movimientoCuenta.getCuenta().getTitularSecundario().elementAt(CONTADOR_CONSORCIO));
//			
//			lstElementos = Arrays.asList(new String[]{
//					cortarPalabra(titularSecundario.getTipoIdentificacion() != null ?
//						(titularSecundario.getTipoIdentificacion().equalsIgnoreCase(VACIO) ?
//						ESPACIO : titularSecundario.getTipoIdentificacion()) : VACIO, 12), 
//					cortarPalabra(titularSecundario.getTipoIdentificacion() == null ? 
//						(titularSecundario.getNumeroIdentificacion() == null ? 
//						ESPACIO : titularSecundario.getNumeroIdentificacion()) : (titularSecundario.getTipoIdentificacion().equalsIgnoreCase(NI) ? 
//						(titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : 
//						titularSecundario.getNumeroIdentificacion().substring(0, titularSecundario.getNumeroIdentificacion().length() - 1)) : 
//						(titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : titularSecundario.getNumeroIdentificacion())), 24), 
//					cortarPalabra(titularSecundario.getTipoIdentificacion() == null ? ESPACIO : 
//						(titularSecundario.getTipoIdentificacion().equalsIgnoreCase(NI) ? (titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : 
//						titularSecundario.getNumeroIdentificacion().substring((titularSecundario.getNumeroIdentificacion().length() - 1))) : 
//						(titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : ESPACIO)), 3), 
//					cortarPalabra(titularSecundario.getNombre()==null? VACIO:titularSecundario.getNombre().equalsIgnoreCase(VACIO) ? ESPACIO : 
//						titularSecundario.getNombre(), 83)});
//			lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
//			addFila(lstCeldas, anchos, ANCHO_SECCIONES);
//			CONTADOR_CONSORCIO++;
//			
//			if (movimientoCuenta.getCuenta().getTitularSecundario().size()<2){
//				titularSecundario = new TitularSecundario();
//				lstElementos = Arrays.asList(new String[]{
//						cortarPalabra(titularSecundario.getTipoIdentificacion() != null ? 
//							(titularSecundario.getTipoIdentificacion().equalsIgnoreCase(VACIO) ? 
//									ESPACIO : titularSecundario.getTipoIdentificacion()) : ESPACIO, 12), 
//						cortarPalabra(titularSecundario.getTipoIdentificacion() == null ? (titularSecundario.getNumeroIdentificacion() == null ? 
//								ESPACIO : titularSecundario.getNumeroIdentificacion()) : (titularSecundario.getTipoIdentificacion().equalsIgnoreCase(NI) ? 
//							(titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : 
//							titularSecundario.getNumeroIdentificacion().substring(0, titularSecundario.getNumeroIdentificacion().length() - 1)) : 
//							(titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : titularSecundario.getNumeroIdentificacion())), 24), 
//						cortarPalabra(titularSecundario.getTipoIdentificacion() == null ? ESPACIO : 
//							(titularSecundario.getTipoIdentificacion().equalsIgnoreCase(NI) ? 
//							(titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : 
//							titularSecundario.getNumeroIdentificacion().substring((titularSecundario.getNumeroIdentificacion().length() - 1))) : 
//							(titularSecundario.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? ESPACIO : ESPACIO)), 3), 
//						cortarPalabra(titularSecundario.getNombre()==null? VACIO:titularSecundario.getNombre().equalsIgnoreCase(VACIO) ? ESPACIO : 
//							titularSecundario.getNombre(), 83)});
//				lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
//				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
//				CONTADOR_CONSORCIO = cantidad;
//			}
		}
		LOG.debug("[ReporteF10][consorcioDinamico][Fin]");
		}
	}
	
	/**
	 * Metodo encargado de generar la seccion tres del formulario 10.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void generarSeccionIII() throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][generarSeccionIII][Inicio]");
		List<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		float[] anchos = new float[1];
		anchos[0]=100;
		CellSEC cellTitulo = new CellSEC();
		CellValueSEC titulo = new CellValueSEC(FontStyleSEC.HELVETICA_BOLD_8PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, TITULO_SECCION_III);
		cellTitulo.setValue(titulo);
		lstCeldas.add(cellTitulo);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[3];
		anchos[0]=43;
		anchos[1]=27;
		anchos[2]=30;
		List<String> lstElementos = Arrays.asList(new String[]{LB_14_NOMBRE_BANCO, LB_15_CODIGO_PAIS, LB_16_CODIGO_CIUDAD});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{cortarPalabra(movimientoCuenta.getCuenta().getBanco().getCodigo().equalsIgnoreCase(VACIO) ? 
//				VACIO : ((movimientoCuenta.getCuenta().getBanco().getDescripcion() != null ? movimientoCuenta.getCuenta().getBanco().getDescripcion() : VACIO)), 53), 
//				cortarPalabra(movimientoCuenta.getCuenta().getBanco().getPais().getCodigoIso().equalsIgnoreCase(VACIO) ? VACIO : ((movimientoCuenta.getCuenta().getBanco()
//						.getPais().getDescripcion() != null ? movimientoCuenta.getCuenta().getBanco().getPais().getDescripcion() : VACIO)), 33), 
//				cortarPalabra(movimientoCuenta.getCuenta().getBanco().getCiudad().getCodigo().equalsIgnoreCase(VACIO) ? VACIO : ((movimientoCuenta.getCuenta().getBanco()
//						.getCiudad().getDescripcion() != null ? movimientoCuenta.getCuenta().getBanco().getCiudad().getDescripcion() : VACIO)), 37)});
//		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[2];
		anchos[0]=43;
		anchos[1]=57;
		lstElementos = Arrays.asList(new String[]{LB_17_NUMERO_CUENTA, LB_18_CODIGO_MONEDA});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{cortarPalabra(movimientoCuenta.getCuenta().getNumeroCuenta().equalsIgnoreCase(VACIO) ? VACIO : movimientoCuenta.getCuenta().getNumeroCuenta(), 53), 
//				cortarPalabra(movimientoCuenta.getCuenta().getMoneda().getCodigo().equalsIgnoreCase(VACIO) ? VACIO
//						: (movimientoCuenta.getCuenta().getMoneda().getCodigo() + (movimientoCuenta.getCuenta().getMoneda().getDescripcion() != null ? GUION + movimientoCuenta.getCuenta().getMoneda().getDescripcion() : VACIO)), 71)});
//		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		LOG.debug("[ReporteF10][generarSeccionIII][Fin]");
	}
	
	/**
	 * Metodo encargado de generar la seccion cuatro del formulario 10.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void generarSeccionIV() throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][generarSeccionIV][Inicio]");
		List<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		float[] anchos = new float[1];
		anchos[0]=100;
		CellSEC cellTitulo = new CellSEC();
		CellValueSEC titulo = new CellValueSEC(FontStyleSEC.HELVETICA_BOLD_8PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, TITULO_SECCION_IV);
		cellTitulo.setValue(titulo);
		lstCeldas.add(cellTitulo);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[3];
		anchos[0]=28;
		anchos[1]=2;
		anchos[2]=70;
		//List<String> lstElementos = Arrays.asList(new String[]{LB_19_SIN_MOVIMIENTO, cortarPalabra(movimientoCuenta.getSinMovimiento().equalsIgnoreCase(CONFIRMACION) ? SELECCION : VACIO, 2), VACIO});
		List<Integer> lstBordes = Arrays.asList(new Integer[] { BorderStyleSEC.NO_BORDER, BorderStyleSEC.ALL_BORDER, BorderStyleSEC.NO_BORDER});
		//lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, lstBordes);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		seccionA(5);
		seccionB(2);
		seccionC();
		seccionD(3);
		LOG.debug("[ReporteF10][generarSeccionIV][Fin]");
	}

	/**
	 * Metodo para generar la seccion A del apartado cuatro.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void seccionA(int cantidad) throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][seccionA][Inicio]");
		List<CellSEC> lstCeldas;
		float[] anchos;
		List<String> lstElementos;
		anchos = new float[1];
//		egresos = new Vector<DetalleMovimiento>();
//		ingresos = new Vector<DetalleMovimiento>();
//		DetalleMovimiento detalleMovimientoIngreso = null;
//		DetalleMovimiento detalleMovimientoEgreso = null;
		anchos[0]=100;
		lstElementos = Arrays.asList(new String[]{SUBTITULO_A});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_BOLD_8PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[4];
		anchos[0]=30;
		anchos[1]=23;
		anchos[2]=23;
		anchos[3]=24;
		lstElementos = Arrays.asList(new String[]{LB_20_NUMERAL_INGRESO, LB_21_VALOR_INGRESO, LB_22_NUMERAL_EGRESO, LB_23_VALOR_EGRESO});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		try {
			// Clasifico los numerales por ingreso y egreso en dos vectores.
//			for (int k = 0; k != movimientoCuenta.getDetalleMovimiento().size(); k++) {
//				DetalleMovimiento detalleMovimiento = (DetalleMovimiento) (movimientoCuenta.getDetalleMovimiento().elementAt(k));
//				if (detalleMovimiento.getIngresoEgreso().equalsIgnoreCase(INGRESO)) {
//					ingresos.addElement(detalleMovimiento);
//				} else {
//					egresos.addElement(detalleMovimiento);
//				}
//			}
		} catch (NullPointerException npe) {
		}
		if(cantidad != 5){
//			if(ingresos.size() > egresos.size()){
//				cantidad = ingresos.size();
//			}else{
//				cantidad = egresos.size();
//			}
		}
		if(cantidad == 0){
			while(CONTADOR_A < 5){
				lstElementos = Arrays.asList(new String[]{
						cortarPalabra(ESPACIO, 35), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 27)});
				List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT)});
				lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
				CONTADOR_A++;
			}
		} else{
			while(CONTADOR_A < cantidad){
//				if(ingresos.size() > CONTADOR_A){
//					detalleMovimientoIngreso = (ingresos.isEmpty() ? new DetalleMovimiento() : (DetalleMovimiento) ingresos.elementAt(CONTADOR_A));
//				}else{
//					detalleMovimientoIngreso = new DetalleMovimiento();
//				}
//				if(egresos.size() > CONTADOR_A){
//					detalleMovimientoEgreso =  (egresos.isEmpty() ? new DetalleMovimiento() : (DetalleMovimiento) egresos.elementAt(CONTADOR_A));				
//				}else{
//					detalleMovimientoEgreso = new DetalleMovimiento();
//				}
//				lstElementos = Arrays.asList(new String[]{
//						cortarPalabra(detalleMovimientoIngreso != null ? (detalleMovimientoIngreso.getNumeral() == 0 ? ESPACIO : 
//							String.valueOf(detalleMovimientoIngreso.getNumeral()) + (detalleMovimientoIngreso.devolverNumeral().getDescripcion() != null ? 
//									GUION + (detalleMovimientoIngreso.devolverNumeral().getDescripcion().length() > 34 ? 
//											detalleMovimientoIngreso.devolverNumeral().getDescripcion().substring(0, 34) : 
//												detalleMovimientoIngreso.devolverNumeral().getDescripcion()) : ESPACIO)) : ESPACIO, 35), 
//						cortarPalabra(detalleMovimientoIngreso != null ? (detalleMovimientoIngreso.getValorLargo().compareTo(new BigDecimal(NUEVO_BIGD_PUNTO)) == 0 ? 
//								ESPACIO : UtilidadSec.formatearValorDecimalConPuntos(detalleMovimientoIngreso.getValorLargo())) : 
//									ESPACIO, 26), 
//						cortarPalabra(detalleMovimientoEgreso != null ? (detalleMovimientoEgreso.getNumeral() == 0 ? ESPACIO : 
//							String.valueOf(detalleMovimientoEgreso.getNumeral()) + (detalleMovimientoEgreso.devolverNumeral().getDescripcion() != null ? 
//									GUION + (detalleMovimientoEgreso.devolverNumeral().getDescripcion().length() > 29 ? 
//											detalleMovimientoEgreso.devolverNumeral().getDescripcion().substring(0, 29) : 
//												detalleMovimientoEgreso.devolverNumeral().getDescripcion()) : ESPACIO)) : ESPACIO, 26), 
//						cortarPalabra(detalleMovimientoEgreso != null ? (detalleMovimientoEgreso.getValorLargo().compareTo(new BigDecimal(NUEVO_BIGD_PUNTO)) == 0 ? 
//								ESPACIO : UtilidadSec.formatearValorDecimalConPuntos(detalleMovimientoEgreso.getValorLargo())) : 
//									ESPACIO, 27)});
//			List<String> lstAlingVal = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.RIGHT),String.valueOf(AlignTextSEC.LEFT),String.valueOf(AlignTextSEC.RIGHT)});
//			lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAlingVal);
//				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
//				CONTADOR_A++;
			}
		}
		LOG.debug("[ReporteF10][seccionA][Fin]");
	}
	
	/**
	 * Metodo para generar la seccion B del apartado cuatro.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void seccionB(int contador) throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][seccionB][Inicio]");
		List<CellSEC> lstCeldas;
		float[] anchos;
		List<String> lstElementos;
		anchos = new float[1];
//		ingresosDevolucion = new Vector<DetalleDevolucion>();
//		egresosDevolucion = new Vector<DetalleDevolucion>();
//		DetalleDevolucion detalleDevolucionIngreso = null;
//		DetalleDevolucion detalleDevolucionEgreso = null;
		anchos[0]=100;
		lstElementos = Arrays.asList(new String[]{SUBTITULO_B});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_BOLD_8PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[4];
		anchos[0]=30;
		anchos[1]=23;
		anchos[2]=23;
		anchos[3]=24;
		lstElementos = Arrays.asList(new String[]{LB_24_NUMERAL_EGRESO, LB_25_VALOR_INGRESO, LB_26_NUMERAL_INGRESO, LB_27_VALOR_EGRESO});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		try {
//			for (int k = 0; k != movimientoCuenta.getDetalleDevoluciones().size(); k++) {
//				DetalleDevolucion detalleDevolucion = (DetalleDevolucion) (movimientoCuenta.getDetalleDevoluciones().elementAt(k));
//				if (detalleDevolucion.getNumeral().getEsIngresoEgreso().equalsIgnoreCase(INGRESO)) {
//					ingresosDevolucion.addElement(detalleDevolucion);
//				} else {
//					egresosDevolucion.addElement(detalleDevolucion);
//				}
//			}
		} catch (NullPointerException npe) {
		}
		if(contador != 2){
//			if(ingresosDevolucion.size() > egresosDevolucion.size()){
//				contador = ingresosDevolucion.size();
//			}else{
//				contador = egresosDevolucion.size();
//			}
		}
		
		if(contador == 0){
			while(CONTADOR_B < 2){
				lstElementos = Arrays.asList(new String[]{
						cortarPalabra(ESPACIO, 35), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 27)});
				List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT)});
				lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
				CONTADOR_B++;
			}
		} else{
			while(CONTADOR_B < contador){
//				if(ingresosDevolucion.size() > CONTADOR_B){
//					detalleDevolucionIngreso = ingresosDevolucion.isEmpty()? new DetalleDevolucion(): (DetalleDevolucion) ingresosDevolucion.elementAt(CONTADOR_B);
//				}else{
//					detalleDevolucionIngreso = new DetalleDevolucion();
//				}
//				if(egresosDevolucion.size() > CONTADOR_B){
//					detalleDevolucionEgreso =  egresosDevolucion.isEmpty()? new DetalleDevolucion(): (DetalleDevolucion) egresosDevolucion.elementAt(CONTADOR_B);				
//				}else{
//					detalleDevolucionEgreso =  new DetalleDevolucion();
//				}
//				lstElementos = Arrays.asList(new String[]{
//						cortarPalabra(detalleDevolucionIngreso.getNumeral() != null ? (detalleDevolucionIngreso.getNumeral().getNumeral() == 0 ? ESPACIO : 
//							String.valueOf(detalleDevolucionIngreso.getNumeral().getNumeral()) + GUION + 
//							(detalleDevolucionIngreso.getNumeral().getDescripcion() != null ? 
//									(detalleDevolucionIngreso.getNumeral().getDescripcion().length() > 34 ? 
//											detalleDevolucionIngreso.getNumeral().getDescripcion().substring(0, 34) : 
//												detalleDevolucionIngreso.getNumeral().getDescripcion()) : ESPACIO)) : ESPACIO, 35), 
//						cortarPalabra(detalleDevolucionIngreso.getNumeral() != null ?
//								UtilidadSec.formatearValorDecimalConPuntos(detalleDevolucionIngreso.getNumeral().getValorLargo()) : ESPACIO, 26), 
//						cortarPalabra(detalleDevolucionEgreso.getNumeral() != null ? (detalleDevolucionEgreso.getNumeral().getNumeral() == 0 ? ESPACIO : 
//							String.valueOf(detalleDevolucionEgreso.getNumeral().getNumeral()) + GUION + 
//							(detalleDevolucionEgreso.getNumeral().getDescripcion() != null ? 
//									(detalleDevolucionEgreso.getNumeral().getDescripcion().length() > 29 ? 
//											detalleDevolucionEgreso.getNumeral().getDescripcion().subSequence(0, 29) : 
//												detalleDevolucionEgreso.getNumeral().getDescripcion()): ESPACIO)) : ESPACIO, 26), 
//						cortarPalabra(detalleDevolucionEgreso.getNumeral() != null ? 
//								UtilidadSec.formatearValorDecimalConPuntos(detalleDevolucionEgreso.getNumeral().getValorLargo()) : ESPACIO, 27)});
//			List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.RIGHT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.RIGHT)});
//				lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
//				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
//				CONTADOR_B++;
			}
		}
		
		if(CONTADOR_B <= 2){
			lstElementos = Arrays.asList(new String[]{LB_28_SALDO_ANTERIOR, LB_29_INGRESOS, LB_30_EGRESOS, LB_31_NUEVO_SALDO});
			lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
			addFila(lstCeldas, anchos, ANCHO_SECCIONES);
			
//			lstElementos = Arrays.asList(new String[]{
//				cortarPalabra(UtilidadSec.formatearValorDecimalConPuntos(movimientoCuenta.getSaldoAnterior()), 35), 
//				cortarPalabra(movimientoCuenta.getIngresosPeriodo().compareTo(new BigDecimalSEC(NUEVO_BIGD_CERO)) == 1 ? 
//					UtilidadSec.formatearValorDecimalConPuntos(movimientoCuenta.getIngresosPeriodo()) : NUEVO_BIGD_PUNTO, 26), 
//				cortarPalabra(movimientoCuenta.getEgresosPeriodo().compareTo(new BigDecimalSEC(NUEVO_BIGD_CERO)) == 1 ? 
//					UtilidadSec.formatearValorDecimalConPuntos(movimientoCuenta.getEgresosPeriodo()) : NUEVO_BIGD_PUNTO, 26), 
//				cortarPalabra(UtilidadSec.formatearValorDecimalConPuntos(movimientoCuenta.getNuevoSaldo()), 27)});
			List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.RIGHT), String.valueOf(AlignTextSEC.RIGHT), String.valueOf(AlignTextSEC.RIGHT), String.valueOf(AlignTextSEC.RIGHT)});
			lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER,lstAling);
			addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		}
		LOG.debug("[ReporteF10][seccionB][Fin]");
	}
	
	/**
	 * Metodo para generar la seccion C del apartado cuatro.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void seccionC() throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][seccionC][Inicio]");
		List<CellSEC> lstCeldas;
		float[] anchos;
		List<String> lstElementos;
		anchos = new float[1];
		anchos[0]=100;
		lstElementos = Arrays.asList(new String[]{SUBTITULO_C});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_BOLD_8PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[2];
		anchos[0]=50;
		anchos[1]=50;
		lstElementos = Arrays.asList(new String[]{LB_32_NUEVO_SALDO_INV_FINANCIERA, LB_33_OVERNIGHT});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{
//			cortarPalabra(movimientoCuenta.getNuevoSaldoInv().compareTo(new BigDecimalSEC(NUEVO_BIGD_CERO)) == 1 ? 
//				UtilidadSec.formatearValorDecimalConPuntos(movimientoCuenta.getNuevoSaldoInv()) : ESPACIO, 62), 
//			cortarPalabra(movimientoCuenta.getOvernight().compareTo(new BigDecimalSEC(NUEVO_BIGD_CERO)) == 1 ? 
//				UtilidadSec.formatearValorDecimalConPuntos(movimientoCuenta.getOvernight()) : ESPACIO, 62)});
		List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT)});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		LOG.debug("[ReporteF10][seccionC][Fin]");
	}
	
	/**
	 * Metodo para generar la seccion D del apartado cuatro.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void seccionD(int contador) throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][seccionD][Inicio]");
		List<CellSEC> lstCeldas;
		float[] anchos;
		List<String> lstElementos;
		addFilaVacia(1.5f);
		CellValueSEC cellParrafo = new CellValueSEC(FontStyleSEC.HELVETICA_BOLD_8PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, SUBTITULO_D); 			
		addParagraph (cellParrafo, FontStyleSEC.INTERLINEADO_7PTS);
		
		anchos = new float[6];
		anchos[0]=10;
		anchos[1]=20;
		anchos[2]=3;
		anchos[3]=20;
		anchos[4]=23;
		anchos[5]=24;
		lstElementos = Arrays.asList(new String[]{LB_34_TIPO, LB_35_NUMERO_IDENTIFICACION, LB_35_DV, LB_36_CODIGO_BR, LB_37_NUMERAL, LB_38_VALOR});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		if(contador == 0){
			while(CONTADOR_D < 3){
				lstElementos = Arrays.asList(new String[]{
						cortarPalabra(ESPACIO, 35), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 26), 
						cortarPalabra(ESPACIO, 27)});
				List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT)});
				lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
				CONTADOR_D++;
			}
		} else{
			while(CONTADOR_D < contador){
//				DetalleCompraVentaDivisas detalleCompraVenta = movimientoCuenta.getDetalleCompraVenta().isEmpty() ? new DetalleCompraVentaDivisas() 
//						: (movimientoCuenta.getDetalleCompraVenta().size() > CONTADOR_D 
//								? (DetalleCompraVentaDivisas) movimientoCuenta.getDetalleCompraVenta().elementAt(CONTADOR_D) 
//										: new DetalleCompraVentaDivisas());
//				lstElementos = Arrays.asList(new String[]{
//						cortarPalabra(detalleCompraVenta.getTipoIdentificacion() != null ? detalleCompraVenta.getTipoIdentificacion() : ESPACIO, 12), 
//						cortarPalabra(detalleCompraVenta.getTipoIdentificacion() != null ? (detalleCompraVenta.getTipoIdentificacion().equalsIgnoreCase(NI) ? 
//								((detalleCompraVenta.getNumeroIdentificacion() == null ? ESPACIO : detalleCompraVenta.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? 
//										ESPACIO : detalleCompraVenta.getNumeroIdentificacion().substring(0, detalleCompraVenta.getNumeroIdentificacion().length() - 1))) : 
//											((detalleCompraVenta.getNumeroIdentificacion() == null ? ESPACIO : detalleCompraVenta.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? 
//													ESPACIO : detalleCompraVenta.getNumeroIdentificacion()))) : ESPACIO, 24), 
//						cortarPalabra(detalleCompraVenta.getTipoIdentificacion() != null ? (detalleCompraVenta.getTipoIdentificacion().equalsIgnoreCase(NI) ? 
//								((detalleCompraVenta.getNumeroIdentificacion() == null ? ESPACIO : detalleCompraVenta.getNumeroIdentificacion().equalsIgnoreCase(VACIO) ? 
//										ESPACIO : detalleCompraVenta.getNumeroIdentificacion().substring((detalleCompraVenta.getNumeroIdentificacion().length() - 1)))) : 
//											((detalleCompraVenta.getNumeroIdentificacion() == null ? ESPACIO : detalleCompraVenta.getNumeroIdentificacion().equalsIgnoreCase(ESPACIO) ? 
//													ESPACIO : ESPACIO))) : ESPACIO, 3), 
//						cortarPalabra(detalleCompraVenta.getNumInterno() > 0 ? String.valueOf(detalleCompraVenta.getNumInterno()) : ESPACIO, 24), 
//						cortarPalabra(detalleCompraVenta.getNumeral() > 0 ? String.valueOf(detalleCompraVenta.getNumeral()) + GUION + 
//								(detalleCompraVenta.devolverNumeral().getDescripcion() != null ? (detalleCompraVenta.devolverNumeral().getDescripcion().length() > 28 ? 
//										detalleCompraVenta.devolverNumeral().getDescripcion().subSequence(0, 26) : detalleCompraVenta.devolverNumeral().getDescripcion()) : 
//											ESPACIO) : ESPACIO, 26), 
//						cortarPalabra(detalleCompraVenta.getNumeral() == 1601 ? UtilidadSec.formatearValorDecimalConPuntos(detalleCompraVenta.getValorLargo()) : 
//							(detalleCompraVenta.getValorLargo().compareTo(new BigDecimalSEC(NUEVO_BIGD_CERO)) == 1 ? 
//									UtilidadSec.formatearValorDecimalConPuntos(detalleCompraVenta.getValorLargo()) : ESPACIO), 28)});
//			List<String> lstAling = Arrays.asList(new String[] { String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.LEFT), String.valueOf(AlignTextSEC.RIGHT)});
//				lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER, lstAling);
//				addFila(lstCeldas, anchos, ANCHO_SECCIONES);
//				CONTADOR_D++;
			}
		}
		LOG.debug("[ReporteF10][seccionD][Fin]");
	}
	
	/**
	 * Metodo encargado de generar la seccion cinco del formulario 10.
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void generarSeccionV() throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][generarSeccionV][Inicio]");
		//RepresentanteLegal representanteLegal = movimientoCuenta.getCuenta().getTitular().getRepresentanteLegal();
		CellValueSEC cellParrafo = new CellValueSEC(FontStyleSEC.HELVETICA_7PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, LB_38_PARRAFO); 			
		addParagraph (cellParrafo, FontStyleSEC.INTERLINEADO_7PTS);
		addFilaVacia(ESPACIO_ENTRE_SECCIONES/2);
		
		ArrayList<CellSEC> lstCeldas = new ArrayList<CellSEC>();
		float[] anchos = new float[1];
		anchos[0]=100;
		CellSEC cellTitulo = new CellSEC();
		CellValueSEC titulo = new CellValueSEC(FontStyleSEC.HELVETICA_BOLD_8PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, TITULO_SECCION_V);
		cellTitulo.setValue(titulo);
		lstCeldas.add(cellTitulo);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[4];
		anchos[0]=10;
		anchos[1]=20;
		anchos[2]=40;
		anchos[3]=30;
		List<String> lstElementos = Arrays.asList(new String[]{LB_39_TIPO, LB_40_NUMERO_IDENTIFICACION, LB_41_NOMBRE, LB_42_FIRMA});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{cortarPalabra(representanteLegal != null && representanteLegal.getTipoIdentificacion() != null && !representanteLegal.getTipoIdentificacion().equalsIgnoreCase(VACIO) ? representanteLegal
//				.getTipoIdentificacion() : VACIO, 12), 
//				cortarPalabra(representanteLegal.getIdentificacion().equalsIgnoreCase(VACIO) ? VACIO : representanteLegal.getIdentificacion(), 24), 
//				cortarPalabra(representanteLegal != null && representanteLegal.getNombre() != null && !representanteLegal.getNombre().equalsIgnoreCase(VACIO) ? UtilidadSec.cortarCadena(representanteLegal.getNombre(), 57) : VACIO, 48), 
//				cortarPalabra(VR_42_FIRMA, 37)});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
		anchos = new float[4];
		anchos[0]=30;
		anchos[1]=23;
		anchos[2]=17;
		anchos[3]=30;
		lstElementos = Arrays.asList(new String[]{LB_43_DIRECCION_NOTIFICACION, LB_44_CODIGO_CIUDAD, LB_45_TELEFONO, LB_46_CORREO_ELECTRONICO});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.NO_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		
//		lstElementos = Arrays.asList(new String[]{cortarPalabra(representanteLegal != null && representanteLegal.getDireccion() != null && !representanteLegal.getDireccion().equalsIgnoreCase(VACIO) ? UtilidadSec.cortarCadena(representanteLegal.getDireccion(), 60) : VACIO, 37),
//				cortarPalabra(representanteLegal.getCiudad().getCodigo().equalsIgnoreCase(VACIO) ? VACIO
//						: (representanteLegal.getCiudad().getCodigo() + (representanteLegal.getCiudad().getDescripcion() != null ? GUION + representanteLegal.getCiudad().getDescripcion() : VACIO)), 26), 
//				cortarPalabra(representanteLegal != null && representanteLegal.getTelefono() != null && !representanteLegal.getTelefono().equalsIgnoreCase(VACIO) ? representanteLegal.getTelefono() : VACIO, 20), 
//				cortarPalabra(representanteLegal != null && representanteLegal.getEmail() != null && !representanteLegal.getEmail().equalsIgnoreCase(VACIO) ? UtilidadSec.cortarCadena(representanteLegal.getEmail(), 48) : VACIO, 37)});
		lstCeldas = agregarValores(lstElementos, FontStyleSEC.HELVETICA_7PTS, BorderStyleSEC.ALL_BORDER);
		addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		LOG.debug("[ReporteF10][generarSeccionV][Fin]");
	}
	
	/**
	 * Metodo encargado de gestionar las secciones dinamicas del pdf. 
	 * @throws DocumentException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	private void seccionesDinamicas() throws DocumentException, MalformedURLException, IOException {
		LOG.debug("[ReporteF10][seccionesDinamicas][Inicio]");
//		CANTIDAD_A =  ingresos.size();
//		CANTIDAD_A_II = egresos.size();
//		CANTIDAD_B = ingresosDevolucion.size();
//		CANTIDAD_B_II = egresosDevolucion.size();
//		CANTIDAD_D = movimientoCuenta.getDetalleCompraVenta().size();
//		CANTIDAD_CONSORCIO = movimientoCuenta.getCuenta().getTitularSecundario().size();
		
		if(CANTIDAD_CONSORCIO > 2 || CANTIDAD_A > 5 || CANTIDAD_A_II > 5 || CANTIDAD_B > 2 
				|| CANTIDAD_B_II > 2 || CANTIDAD_D > 3){
			documentPDF.newPage();
		}
		
		if(CANTIDAD_CONSORCIO > 2){
			consorcioDinamico(CANTIDAD_CONSORCIO);
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
		}
		if(CANTIDAD_A_II > 5 || CANTIDAD_A > 5 || CANTIDAD_B > 2 || CANTIDAD_B_II > 2 || CANTIDAD_D > 3){
			List<CellSEC> lstCeldas = new ArrayList<CellSEC>();
			float[] anchos = new float[1];
			anchos[0]=100;
			CellSEC cellTitulo = new CellSEC();
			CellValueSEC titulo = new CellValueSEC(FontStyleSEC.HELVETICA_BOLD_8PTS, AlignTextSEC.LEFT,BorderStyleSEC.NO_BORDER, TITULO_SECCION_IV);
			cellTitulo.setValue(titulo);
			lstCeldas.add(cellTitulo);
			addFila(lstCeldas, anchos, ANCHO_SECCIONES);
		}
		if(CANTIDAD_A > 5){
			seccionA(CANTIDAD_A);
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
		} else if(CANTIDAD_A_II > 5) {
			seccionA(CANTIDAD_A_II);
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);			
		}
			
		if(CANTIDAD_B > 2){
			seccionB(CANTIDAD_B);
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
		} else if(CANTIDAD_B_II > 2) {
			seccionB(CANTIDAD_B_II);
			addFilaVacia(ESPACIO_ENTRE_SECCIONES);
		}
		
		if(CANTIDAD_D > 3){
			seccionD(CANTIDAD_D);
		}
		LOG.debug("[ReporteF10][seccionesDinamicas][Fin]");
	}

	/**
	 * getFechaCircular
	 * @return
	 */
	public String getFechaCircular() {
		return fechaCircular;
	}
	/**
	 * setFechaCircular
	 * @param fechaCircular
	 */
	public void setFechaCircular(String fechaCircular) {
		this.fechaCircular = fechaCircular;
	}
	/**
	 * getMovimientoCuenta
	 * @return
	 */
//	public MovimientoCuenta getMovimientoCuenta() {
//		return movimientoCuenta;
//	}
//	/**
//	 * setMovimientoCuenta
//	 * @param movimientoCuenta
//	 */
//	public void setMovimientoCuenta(MovimientoCuenta movimientoCuenta) {
//		this.movimientoCuenta = movimientoCuenta;
//	}
	/**
	 * getNombreArchivo
	 * @return
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * setNombreArchivo
	 * @param nombreArchivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return
	 */
	public boolean isEsInterno() {
		return esInterno;
	}

	/**
	 * @param esInterno
	 */
	public void setEsInterno(boolean esInterno) {
		this.esInterno = esInterno;
	}
	
		
}