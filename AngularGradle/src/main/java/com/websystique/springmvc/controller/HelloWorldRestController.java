package com.websystique.springmvc.controller;
 
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.journaldev.mongodb.dao.MongoDBPersonDAO;
import com.journaldev.mongodb.dao.MongoDBUserDAO;
import com.journaldev.mongodb.listener.MongoDBContextListener;
import com.journaldev.mongodb.model.Person;
import com.journaldev.mongodb.utils.LectorPropiedades;
import com.journaldev.mongodb.utils.exceptions.ExcepcionConfiguracion;
import com.journaldev.mongodb.utils.properties.ConstantesGenerales;
import com.mongodb.MongoClient;
import com.websystique.springmvc.model.User;
import com.websystique.springmvc.service.UserService;
 
/**
 * 
 * @author jaroman
 *
 */
@RestController
public class HelloWorldRestController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	private final static Logger LOGGER = Logger.getLogger(HelloWorldRestController.class);
 
    @Autowired
    UserService userService;  //Service which will do all data retrieval/manipulation work
 
    
    /**
     * Lee todos los usuarios 
     * @param request
     * @param response
     * @return
     * @throws ExcepcionConfiguracion
     */
    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers(HttpServletRequest request,
            HttpServletResponse response) throws ExcepcionConfiguracion {

    	LOGGER.debug("[HelloWorldRestController][listAllUsers][INICIO]");
    	MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute(ConstantesGenerales.MONGO_CLIENT);
		MongoDBUserDAO userDAO = new MongoDBUserDAO(mongo);
		List<User> users = userDAO.readAllUser();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        LOGGER.debug("[HelloWorldRestController][listAllUsers][FIN]");
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
 
    /**
     * consulta usuario por id 
     * @param id
     * @return usuario
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") String id) {
        System.out.println("Fetching User with id " + id);
        LOGGER.debug("[HelloWorldRestController][getUser][INICIO]");
        User user = userService.findById(id);
        if (user == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        LOGGER.debug("[HelloWorldRestController][getUser][FIN]");
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
 
    /**
     * Metodo que crea usuario en base de datos 
     * @param user
     * @param ucBuilder
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder, HttpServletRequest request,
            HttpServletResponse response) {
        System.out.println("Creating User " + user.getUsername());
        LOGGER.debug("[HelloWorldRestController][createUser][INICIO]");
        MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute(ConstantesGenerales.MONGO_CLIENT);
		MongoDBUserDAO userDAO = new MongoDBUserDAO(mongo);
		userDAO.createUser(user);
		
        if (userService.isUserExist(user)) {
            System.out.println("A User with name " + user.getUsername() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        userService.saveUser(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        LOGGER.debug("[HelloWorldRestController][createUser][FIN]");
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
 
    /**
     * Metodo para actualiza usuario en base de datos 
     * @param id
     * @param user
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") String id, @RequestBody User user, HttpServletRequest request,
            HttpServletResponse response) {
        System.out.println("Updating User " + id);
        LOGGER.debug("[HelloWorldRestController][updateUser][INICIO]");
        MongoClient mongo = (MongoClient) request.getServletContext()
				.getAttribute(ConstantesGenerales.MONGO_CLIENT);
		MongoDBUserDAO userDAO = new MongoDBUserDAO(mongo);
		userDAO.updatePerson(user);
        User currentUser = userService.findById(id);
        if (currentUser==null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        currentUser.setUsername(user.getUsername());
        currentUser.setAddress(user.getAddress());
        currentUser.setEmail(user.getEmail());
        userService.updateUser(currentUser);
        LOGGER.debug("[HelloWorldRestController][updateUser][FIN]");
        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }
 
    /**
     * Metodo que elimina usuario en base de datos 
     * @param id
     * @return
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") String id) {
        System.out.println("Fetching & Deleting User with id " + id);
        LOGGER.debug("[HelloWorldRestController][deleteUser][INICIO]");
        User user = userService.findById(id);
        if (user == null) {
            System.out.println("Unable to delete. User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userService.deleteUserById(id);
        LOGGER.debug("[HelloWorldRestController][deleteUser][FIN]");
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
 
    /**
     * Metodo que borra todos los usuario en base de datos 
     * @return
     */
    @RequestMapping(value = "/user/", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteAllUsers() {
        System.out.println("Deleting All Users");
        LOGGER.debug("[HelloWorldRestController][deleteAllUsers][INICIO]");
        userService.deleteAllUsers();
        LOGGER.debug("[HelloWorldRestController][deleteAllUsers][FIN]");
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
 
}