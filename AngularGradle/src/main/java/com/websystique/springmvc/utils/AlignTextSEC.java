package com.websystique.springmvc.utils;

import com.itextpdf.text.pdf.PdfPCell;


public class AlignTextSEC 
{

  public static final int LEFT = PdfPCell.ALIGN_LEFT;
  public static final int RIGHT = PdfPCell.ALIGN_RIGHT;
  public static final int CENTER= PdfPCell.ALIGN_CENTER;
  public static final int JUSTIFY = PdfPCell.ALIGN_JUSTIFIED;
  public static final int MIDDLE = PdfPCell.ALIGN_MIDDLE;
  public static final int TOP = PdfPCell.ALIGN_TOP;

  private final int id;


  private AlignTextSEC(int id)
  {
    this.id = id;
  }

  public boolean equal(Object object) 
  {
	  AlignTextSEC s = (AlignTextSEC) object;
	  return this.id == s.getId();
  }

  public int hashCode() 
  {
	  return id;
  }

  public int getId() {
	return id;
  }
  
  
}