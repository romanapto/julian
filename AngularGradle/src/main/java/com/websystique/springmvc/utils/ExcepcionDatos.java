package com.websystique.springmvc.utils;

/**
 * Manejador de excepciones para la base de datos. Creation date: (16/11/2001 14:42:24)
 * 
 * @author: Edgar G�mez
 */
public class ExcepcionDatos extends Exception {
	/**
	 * ExcepcionDatos constructor comment.
	 */
	
	/**
	* CQ- 22332
	* se agrega codigo de error y se encapsula
	 * @author Carlos Gutierrez (Asesoftware)
	 * @param codigoAgente
	 * @param formulario
	 * @param esGracia
	 * @throws ExcepcionDatos
	 */
	
	private int codigoError;
	
	public ExcepcionDatos() {
		super();
	}

	/**
	 * ExcepcionDatos constructor comment.
	 * 
	 * @param s java.lang.String
	 */
	public ExcepcionDatos(String s) {
		super(s);
	}

	/**
	* CQ- 22332
	* se agrega codigo de error y se encapsula
	 * @author Carlos Gutierrez (Asesoftware)
	 * @param codigoAgente
	 * @param formulario
	 * @param esGracia
	 * @throws ExcepcionDatos
	 */
	
	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}
}