package com.websystique.springmvc.utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;

/**
 * Clase encargada de administrar los estilos de letra del documento
 */
public class FontStyleSEC {

	private final int id;
	public static final float NORMAL_SIZE = 7F;

	public static final Font TIMES_ROMAN_9PTS = new Font(FontFamily.TIMES_ROMAN, 9F);
	public static final Font TIMES_ROMAN_BOLD_9PTS = new Font(FontFamily.TIMES_ROMAN, 9F, Font.BOLD);
	public static final Font TIMES_ROMAN_BOLD_7PTS = new Font(FontFamily.TIMES_ROMAN, 7F, Font.BOLD);
	public static final Font TIMES_ROMAN_BOLD_RED_9PTS = new Font(FontFamily.TIMES_ROMAN, 9F, Font.BOLD, BaseColor.RED);
	public static final Font TIMES_ROMAN_BOLD_RED_14PTS = new Font(FontFamily.TIMES_ROMAN, 14F, Font.BOLD,BaseColor.RED);

	public static final Font HELVETICA_6PTS = new Font(FontFamily.HELVETICA, 6F);
	public static final Font HELVETICA_6_5PTS = new Font(FontFamily.HELVETICA, 6.5F);
	public static final Font HELVETICA_BOLD_8PTS = new Font(FontFamily.HELVETICA, 8F, Font.BOLD);
	public static final Font HELVETICA_7PTS = new Font(FontFamily.HELVETICA, 7F);
	public static final float INTERLINEADO_7PTS = 7f;

	private FontStyleSEC(int id) {
		this.id = id;
	}

	public boolean equal(Object object) {
		FontStyleSEC s = (FontStyleSEC) object;
		return this.id == s.getId();
	}

	public int hashCode() {
		return id;
	}

	public int getId() {
		return id;
	}

}