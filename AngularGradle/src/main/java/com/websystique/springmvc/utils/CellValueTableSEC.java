package com.websystique.springmvc.utils;

import java.util.List;

import com.itextpdf.text.Font;

public class CellValueTableSEC extends CellValueSEC {
	
	/**
	 * Lista opcional para casillas que contengan m�s de un t�tulo
	 * y se tenga la necesidad de realizar un rowspan en la celda.
	 * Si la lista de labels contiene m�s de un registro,
	 * se tiene la necesidad de realizar un rowspan de la celda
	 * con la cantidad de cadenas de la lista. Si es nula, se tomar�
	 * el atributo value del objeto.
	 */
	private List titulosTabla;
	
	
	public CellValueTableSEC() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CellValueTableSEC(Font estilo, int align, int borderStyle, Object value) {
		super(estilo, align, borderStyle, value);
		// TODO Auto-generated constructor stub
	}

	public CellValueTableSEC(Font estilo, int borderStyle, Object value) {
		super(estilo, borderStyle, value);
		// TODO Auto-generated constructor stub
	}
	

	public List getTitulosTabla() {
		return titulosTabla;
	}

	public void setTitulosTabla(List titulosTabla) {
		this.titulosTabla = titulosTabla;
	}

	public CellValueTableSEC(Font estilo, int align,
			int borderStyle, int verticalAlign, int rowSpan,
			Object value) {
		super(estilo, align, borderStyle, verticalAlign, rowSpan, value);
		// TODO Auto-generated constructor stub
	}
	
	
	public void setDefaultTitleTable(){
		this.setEstilo(FontStyleSEC.HELVETICA_BOLD_8PTS);
		this.setBorderStyle(BorderStyleSEC.ALL_BORDER);
		this.setAlign(AlignTextSEC.CENTER);
	}
	
	

}
