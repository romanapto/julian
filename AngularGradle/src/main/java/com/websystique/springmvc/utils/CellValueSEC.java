package com.websystique.springmvc.utils;

import com.itextpdf.text.Font;

public class CellValueSEC {

	/**
	 * Estilo del valor de la casilla.
	 */
	private Font estilo = FontStyleSEC.TIMES_ROMAN_9PTS;
	
	/**
	 * Tipo de alineación del valor de la casilla. Por defecto su valor es LEFT.
	 */
	private int align = AlignTextSEC.LEFT;
	
	private int borderStyle = BorderStyleSEC.NO_BORDER;
	
	private int verticalAlign = AlignTextSEC.MIDDLE;
	
	private int rowSpan = 1;
	
	private boolean isImagen = false;
	
	private int padding = 1;

    private float[] fit = new float[]{40, 40};
    
    private int colSpan = 1;
    
    private boolean noWrap = false;
    
	/**
	 * Atributo que contiene el valor que se debe mostrar en la casilla.
	 */
	private Object value;

	
	public boolean isImagen() {
		return isImagen;
	}

	public void setImagen(boolean isImagen) {
		this.isImagen = isImagen;
	}

	public Font getEstilo() {
		return estilo;
	}

	public void setEstilo(Font estilo) {
		this.estilo = estilo;
	}
	
	public int getBorderStyle() {
		return borderStyle;
	}

	public void setBorderStyle(int borderStyle) {
		this.borderStyle = borderStyle;
	}

	public int getAlign() {
		return align;
	}

	public void setAlign(int align) {
		this.align = align;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	public int getVerticalAlign() {
		return verticalAlign;
	}

	public void setVerticalAlign(int verticalAlign) {
		this.verticalAlign = verticalAlign;
	}

	public CellValueSEC(Font estilo, int align,
			int borderStyle, Object value) {
		super();
		this.estilo = estilo;
		this.align = align;
		this.borderStyle = borderStyle;
		this.value = value;
	}

	public CellValueSEC(Font estilo, int borderStyle, Object value) {
		super();
		this.estilo = estilo;
		this.borderStyle = borderStyle;
		this.value = value;
	}
	
	public CellValueSEC(Font estilo, int align,
			int borderStyle, int verticalAlign, int rowSpan,
			Object value) {
		super();
		this.estilo = estilo;
		this.align = align;
		this.borderStyle = borderStyle;
		this.verticalAlign = verticalAlign;
		this.rowSpan = rowSpan;
		this.value = value;
	}
	
	

	public CellValueSEC(Object value) {
		super();
		this.value = value;
	}

	public int getRowSpan() {
		return rowSpan;
	}

	public void setRowSpan(int rowSpan) {
		this.rowSpan = rowSpan;
	}

	public CellValueSEC() {
		super();
	}

	public int getPadding() {
		return padding;
	}

	public void setPadding(int padding) {
		this.padding = padding;
	}

	public float[] getFit() {
		return fit;
	}

	public void setFit(float[] fit) {
		this.fit = fit;
	}

	public int getColSpan() {
		return colSpan;
	}

	public void setColSpan(int colSpan) {
		this.colSpan = colSpan;
	}

	public boolean isNoWrap() {
		return noWrap;
	}

	public void setNoWrap(boolean noWrap) {
		this.noWrap = noWrap;
	}

}
