package com.websystique.springmvc.utils;

/**
 * Manejador de las excepciones de toda la parte de PDF's. Creation date: (2003-08-11 07:21:42 a.m.)
 * 
 * @author: Edgar G�mez
 */
public class ExcepcionPdf extends Exception {
	/**
	 * ExcepcionPdf constructor comment.
	 */
	public ExcepcionPdf() {
		super();
	}

	/**
	 * ExcepcionPdf constructor comment.
	 * 
	 * @param s java.lang.String
	 */
	public ExcepcionPdf(String s) {
		super(s);
	}
}
