package com.websystique.springmvc.utils;

public class RecuadroSEC {
	 private String fechaRadicacion;
	 private String numeroRadicacion;
	 private String numOperacion;
	 private RoundBR roundBR;
	public String getFechaRadicacion() {
		return fechaRadicacion;
	}
	public void setFechaRadicacion(String fechaRadicacion) {
		this.fechaRadicacion = fechaRadicacion;
	}
	public String getNumeroRadicacion() {
		return numeroRadicacion;
	}
	public void setNumeroRadicacion(String numeroRadicacion) {
		this.numeroRadicacion = numeroRadicacion;
	}
	public String getNumOperacion() {
		return numOperacion;
	}
	public void setNumOperacion(String numOperacion) {
		this.numOperacion = numOperacion;
	}
	public RoundBR getRoundBR() {
		return roundBR;
	}
	public void setRoundBR(RoundBR roundBR) {
		this.roundBR = roundBR;
	}
	 
}
