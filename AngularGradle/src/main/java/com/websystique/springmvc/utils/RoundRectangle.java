package com.websystique.springmvc.utils;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;

public class RoundRectangle  implements PdfPCellEvent {
	
	private RoundBR roundBR;
	
	public RoundRectangle () {
		
	}
	
	public RoundRectangle (RoundBR roundBR) {
		this.roundBR = roundBR;
	}

	public void cellLayout(PdfPCell cell, Rectangle rect,
			PdfContentByte[] canvas) {

		// Aqui se debe manejar parametrizado los numeros.
		PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
		
		// Default
		/*
		 * cb.roundRectangle(rect.left() + 1.5f, rect.bottom() + 1.5f,
				rect.width() - 3, rect.height() - 3, 4);
		 * */
		
		cb.roundRectangle(rect.getLeft() + roundBR.getxLeft(), rect.getBottom() + roundBR.getyBottom(),
				rect.getWidth() + roundBR.getWidth(), rect.getHeight() + roundBR.getHeight(), roundBR.getRadius());
		cb.stroke();
	}
}
