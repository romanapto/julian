package com.websystique.springmvc.utils;

public class RoundBR {

	/**
	 * x - x-coordinate of the starting point 
	 */
	private float xLeft;
	
	/**
	 * y - y-coordinate of the starting point
	 */
	private float yBottom;
	
	/**
	 * w - width
	 */
	private float width; 
	
	/**
	 * h - height
	 */
	private float height;
	
	/**
	 * r - radius of the arc corner
	 */
	private float radius = 12;

	public float getxLeft() {
		return xLeft;
	}

	public void setxLeft(float xLeft) {
		this.xLeft = xLeft;
	}

	public float getyBottom() {
		return yBottom;
	}

	public void setyBottom(float yBottom) {
		this.yBottom = yBottom;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
	
}
