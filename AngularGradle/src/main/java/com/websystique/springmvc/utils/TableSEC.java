package com.websystique.springmvc.utils;

import java.util.List;

/**
 * Clase encargada de definir el formato y contenido de las tablas
 * @author cfcorrales
 *
 */

public class TableSEC {
	
	/**
	 * Encabezados de la tabla 
	 */
	private List<EncabezadoTableSEC> encabezados;

	/**
	 * Filas de la tabla
	 */
	private List<RowTableSEC> filas;
	/**
	 * Ancho de las columnas de la tabla
	 */
	private float[] anchoColumnas = {100};
	/**
	 * Ancho de la tabla
	 */
	private float anchoTotal = 100;
	
	private int radio = 1;
	
	
	public float[] getAnchoColumnas() {
		return anchoColumnas;
	}
	public void setAnchoColumnas(float[] anchoColumnas) {
		this.anchoColumnas = anchoColumnas;
	}
	public float getAnchoTotal() {
		return anchoTotal;
	}
	public void setAnchoTotal(float anchoTotal) {
		this.anchoTotal = anchoTotal;
	}
	public List<RowTableSEC> getFilas() {
		return filas;
	}
	public void setFilas(List<RowTableSEC> filas) {
		this.filas = filas;
	}
	
	public List<EncabezadoTableSEC> getEncabezados() {
		return encabezados;
	}
	public void setEncabezados(List<EncabezadoTableSEC> encabezados) {
		this.encabezados = encabezados;
	}
	public int getRadio() {
		return radio;
	}
	public void setRadio(int radio) {
		this.radio = radio;
	}
	
	
	
}
