package com.websystique.springmvc.utils;

/**
 * Clase para administrar las constantes genericas en la aplicaci�n para todos los formularios
 * Creation date: (07/07/2016)
 * @author lfmartinezg
 *
 */
public final class Constantes {
	
	/* TIPOS ENVIOS */
	public static final String ENVIO_XML = "ENVIO_XML";
	public static final String ENVIO_HTML = "envioHtml";
	public static final String IMPRESION = "imp";
	
	public static final String ENVIO_CORRECCION_ERROR_DIGITACION = "envioCED";
	public static final String ENVIO_SOI = "envioSOI";
	public static final String TIPO_ENVIAR = "enviar";
	public static final String TIPO_IMPRIMIR = "imprimir";
	public static final String TIPO_OPER_ICE_FORMULARIO_11 = "C"; //FIXME  CP - ESTA CLASE ES PARA CONSTANTES GENERALES NO PARTICULARES DE UN FORMULARIO.
	public static final String TIPO_IMPRIMIR_F11 = "impresion"; //FIXME  CP - ESTA CLASE ES PARA CONSTANTES GENERALES NO PARTICULARES DE UN FORMULARIO. QUE DIFERENCIA TIENE IMPRESION CON IMPRIMIR ?
	
	public static final String NOMBRE_APLICACION = "SecIntenet";
	public static final String TIPO_USUARIO_1 = "1"; //CP - QUE SIGNIFICA TIPO_USUARIO_1 ? Y TIPO_USUARIO_2?  NOMBRES DICIENTES POR FAVOR!
	public static final String TIPO_USUARIO_2 = "2";
	public static final String EXTENSION_PDF = ".pdf";
	public static final String EXTENSION_XLS = ".xls";
	public static final String ARCHIVO_IMP = "_IMP";
	public static final String EXTENSION_HTML = ".html";
	public static final String EXTENSION_FIRMA = ".p7z";
	public static final String FORMULARIO_3 = "F3";
	public static final String FORMULARIO_3C = "F3C";
	public static final String FORMULARIO_3A = "F3A";
	public static final String FORMULARIO_6 = "F6";
	public static final String FORMULARIO_7 = "F7";
	public static final String FORMULARIO_13 = "F13";
	public static final String USUARIO_BR = "BR";
	public static final int ANCHO_DISPLAY_POPUP_SUCED = 620;
	public static final int ALTO_DISPLAY_POPUP_SUCED = 600;
	public static final String PATRON_SIMPLE_EMAIL = "^[\\w!#$%&'*+/=?{|}~^-]+(?:\\.[\\w!#$%&'*+/=?{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
	public static final String SEPARADOR_LISTA_EMAIL = ",";
	public static final String SEPARADOR_LISTA_OIDS = ",";
	public static final String FORMULARIO_15_BOLSA = "IEEB";
	public static final String FORMULARIO_15 = "F15";
	public static final String FORMULARIO_4 = "F4";
	public static final String FORMULARIO_10 = "F10";
	public static final String FORMULARIO_19 = "F19";
	public static final int CTE_MARGEN_F10 = 2;
	public static final String FORMULARIO_11 = "F11";
	
	public static final String FORMULARIO_8 = "F8";
	public static final String TIPO_OP_INICIAL = "1 Inicial";
	public static final String TIPO_OP_MODIFICACION = "2 Modificaci�n";
	public static final String TIPO_OP_ANULACION = "3 Anulaci�n";
	public static final String IP_CLIENTE_REMOTO = "ipClienteRemoto";
	public static final String RUTA_ARCHIVO = "rutaArchivo";
	public static final String NOMBRE_ARCHIVO = "nombreArchivo";
	public static final String TIPO_ENVIO = "envio";
	public static final String PROVEEDOR_SEGURIDAD_SUCED = "IAIK";
	public static final String SEPARADOR_RUTAS_PORTAFIRMA = ";";
	public static final String IDENTIFICACION_NI = "NI";
	public static final String IDENTIFICACION_CC = "CC";
	public static final String IDENTIFICACION_CE = "CE";
	public static final String IDENTIFICACION_PB = "PB";
	public static final String IDENTIFICACION_RC = "RC";
	
	
	public static final String SI = "Si";
	
	public static final String NO = "No";
	
	public static final String VACIO = "";
	
	
	/**
	 * Constantes para el tama�o maximo de las casillas de los PDF.
	 */
	public static final int CERO = 0;
	public static final int UNO = 1;
	public static final int DOS = 2;
	public static final int TRES = 3;
	public static final int CUATRO = 4;
	public static final int CINCO = 5;
	public static final int SEIS = 6;
	public static final int SIETE = 7;
	public static final int OCHO = 8;
	public static final int NUEVE = 9;
	public static final int DIEZ = 10;
	public static final int ONCE = 11;
	public static final int DOCE = 12;
	public static final int TRECE = 13;
	public static final int CATORCE = 14;
	public static final int QUINCE = 15;
	public static final int DIECISEIS = 16;
	public static final int DIECISIETE = 17;
	public static final int DIECIOCHO = 18;
	public static final int DIECINUEVE = 19;
	public static final int VEINTE = 20;
	public static final int VEINTIUNO = 21;
	public static final int VEINTIDOS = 22;
	public static final int VEINTITRES = 23;
	public static final int VEINTICUATRO = 24;
	public static final int VEINTICINCO = 25;
	public static final int VEINTISEIS = 26;
	public static final int VEINTISIETE = 27;
	public static final int VEINTIOCHO = 28;
	public static final int VEINTINUEVE = 29;
	public static final int TREINTA = 30;
	public static final int TREINTAYUNO = 31;
	public static final int TREINTAYDOS = 32;
	public static final int TREINTAYTRES = 33;
	public static final int TREINTAYCUATRO = 34;
	public static final int TREINTAYCINCO = 35;
	public static final int TREINTAYSEIS = 36;
	public static final int TREINTAYSIETE = 37;
	public static final int TREINTAYOCHO = 38;
	public static final int TREINTAYNUEVE = 39;
	public static final int CUARENTA = 40;
	public static final int CINCUENTAYCUATRO = 54;
	public static final int SETENTAYOCHO = 78;
	public static final int OCHENTA = 80;
	public static final int NOVENTA = 90;
	public static final int CIEN = 100;
	public static final Double CERO_DECIMAL = 0.00d;
	
	public static final String CERTIFICADO_ESTADO_REVOCACION = "Revocado";
	public static final String CERTIFICADO_ESTADO_NO_VALIDO = "No V�lido";
	
}

