package com.websystique.springmvc.utils;


public class CellSEC {

	
	/**
	 * Atributo con la información del Label de la casilla.
	 */
	private  CellValueSEC label;
	
	/**
	 * Atributo con la información del Valor de la casilla.
	 * 
	 */
	private  CellValueSEC value;
	
	

	public CellValueSEC getLabel() {
		return label;
	}

	public void setLabel(CellValueSEC label) {
		this.label = label;
	}

	public CellValueSEC getValue() {
		return value;
	}

	public void setValue(CellValueSEC value) {
		this.value = value;
	}


	
}
