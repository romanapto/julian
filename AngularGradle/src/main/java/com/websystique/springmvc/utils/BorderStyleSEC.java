package com.websystique.springmvc.utils;

import com.itextpdf.text.pdf.PdfPCell;

/**
 * Clase que define la forma de la casilla
 * a nivel de bordes.
 * @author cpenaalv830013774
 *
 */
public class BorderStyleSEC {
	
	public static final int NO_BORDER = PdfPCell.NO_BORDER;
	public static final int ALL_BORDER = PdfPCell.BOX;
	public static final int BOTTOM = PdfPCell.BOTTOM;
  
}