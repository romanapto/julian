package com.websystique.springmvc.utils;


/**
 * Clase encargada de definir el formato y contenido de las tablas
 * @author cfcorrales
 *
 */

public class EncabezadoTableSEC {

	/**
	 * Filas de la tabla
	 */
	private CellValueTableSEC encabezado;
	
	public CellValueTableSEC getEncabezado() {
		return encabezado;
	}

	public void setEncabezado(CellValueTableSEC encabezado) {
		this.encabezado = encabezado;
	}

	
	
}
