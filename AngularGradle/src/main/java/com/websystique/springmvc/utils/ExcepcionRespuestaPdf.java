package com.websystique.springmvc.utils;

/**
 * Manejador de las excepciones para la generaci�n respuesta PDF. Creation date: (2003-08-22 07:59:15 a.m.)
 * 
 * @author: Edgar G�mez
 */
public class ExcepcionRespuestaPdf extends ExcepcionPdf {
	/**
	 * ExcepcionRespuestaPdf constructor comment.
	 */
	public ExcepcionRespuestaPdf() {
		super();
	}

	/**
	 * ExcepcionRespuestaPdf constructor comment.
	 * 
	 * @param s java.lang.String
	 */
	public ExcepcionRespuestaPdf(String s) {
		super(s);
	}
}
