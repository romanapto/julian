package com.websystique.springmvc.utils;

import java.util.List;

public class EncabezadoSEC {

	// 1 Inicial, 2 Modificacion, 3 Anulacion
	private int tipoOperacion;
	
	// 1 Interno , 2 Externo , 3 Sin autenticacion
	private int tipoUsuario;
	
	// Titulo NEgrilla del formulario
	private String tituloPrincipal;
	
	// SubTitulo de circular reglamentaria
	private String tituloSecundario;
	
	// Titulo de Formulario.
	private String tituloFormulario;
	
	
	// INFORMACION DE CUADRO
	
	private RecuadroSEC recuadro;
	
	private List lstSecciones;
	
	/* Por defecto es 0, a medida que se aumente se agranda el rectangulo del cuadro 
	 * de uso exclusivo de la republica*/
	private RoundBR roundBR;

	public int getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(int tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public int getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(int tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getTituloPrincipal() {
		return tituloPrincipal;
	}

	public void setTituloPrincipal(String tituloPrincipal) {
		this.tituloPrincipal = tituloPrincipal;
	}

	public String getTituloSecundario() {
		return tituloSecundario;
	}

	public void setTituloSecundario(String tituloSecundario) {
		this.tituloSecundario = tituloSecundario;
	}


	public RoundBR getRoundBR() {
		return roundBR;
	}

	public void setRoundBR(RoundBR roundBR) {
		this.roundBR = roundBR;
	}

	public String getTituloFormulario() {
		return tituloFormulario;
	}

	public void setTituloFormulario(String tituloFormulario) {
		this.tituloFormulario = tituloFormulario;
	}

	public RecuadroSEC getRecuadro() {
		return recuadro;
	}

	public void setRecuadro(RecuadroSEC recuadro) {
		this.recuadro = recuadro;
	}

	public List getLstSecciones() {
		return lstSecciones;
	}

	public void setLstSecciones(List lstSecciones) {
		this.lstSecciones = lstSecciones;
	}
	
	
	
}
