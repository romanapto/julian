package com.websystique.springmvc.utils;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;

/**
 * Clase encargada de crear el recuadro del encabezado
 * @author cfcorrales
 *
 */
public class RecuadroUtil {
	
	

	
	/**
	 * M�todo encargado de generar un recuadro para el tipo de operaci�n Modificaci�n o Anulaci�n usuario interno
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param cantidadColumnas Cantidad de columnas del recuadro
	 * @param anchoColumnas Ancho en pixeles de cada columna
	 * @param tituloRectangulo T�tulo que aparece en el recuadro
	 * @return Objeto de tipo PdfPCell con el recuadro 
	 * @throws DocumentException Excepci�n generada en caso que se presente un problema insertando en el documento
	 * 
	 */
	public static PdfPCell generarRecuadroInternoModificacionAnulacion(EncabezadoSEC encabezado, int cantidadColumnas, int anchoColumnas[], String tituloRectangulo) throws DocumentException{
		RoundBR round = new RoundBR();
		//round.setRadius(12);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable outter = new PdfPTable(cantidadColumnas);
		outter.setWidths(anchoColumnas);
	    
	    Phrase phrase = new Phrase(tituloRectangulo, FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
	    PdfPCell cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setColspan(3);
	    cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
	    outter.addCell(cell);
	    
	    phrase = new Phrase();
	    cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setColspan(3);
	    cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
	    outter.addCell(cell);
		
	    
	    PdfPTable inner1 = new PdfPTable(1);
	    phrase = new Phrase("Fecha Radicaci�n:", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
	    cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        
	    inner1.addCell(cell);
	    
	    phrase = new Phrase("N�mero Radicaci�n:", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setBorder(Rectangle.NO_BORDER);
        inner1.addCell(cell);
        
        
	    PdfPTable inner2 = new PdfPTable(1);
	    
	    phrase = new Phrase(encabezado.getRecuadro().getFechaRadicacion()!=null?encabezado.getRecuadro().getFechaRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        inner2.addCell(cell);
        
        phrase = new Phrase(encabezado.getRecuadro().getNumeroRadicacion()!=null?encabezado.getRecuadro().getNumeroRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        inner2.addCell(cell);
	    
	    phrase = new Phrase(encabezado.getRecuadro().getNumOperacion()!=null?encabezado.getRecuadro().getNumOperacion():"", FontStyleSEC.TIMES_ROMAN_BOLD_RED_9PTS);
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
        cell.setBorder(Rectangle.NO_BORDER);
        
        PdfPTable inner3 = new PdfPTable(1);
        inner3.addCell(cell);
        
        cell = new PdfPCell(inner1);
        cell.setBorder(Rectangle.NO_BORDER);
	    outter.addCell(cell);
	    cell = new PdfPCell(inner2);
        cell.setBorder(Rectangle.NO_BORDER);
	    outter.addCell(cell);
	    cell = new PdfPCell(inner3);
        cell.setBorder(Rectangle.NO_BORDER);
        outter.addCell(cell);
        
	    cell = new PdfPCell(outter);
	    cell.setPadding(4);
	    cell.setCellEvent(roundRectangle);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
	    
		return cell;
	}
	
	/**
	 * M�todo encargado de generar un recuadro con t�tulo y n�mero de operaci�n  
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param anchoColumnas Ancho en pixeles de cada columna
	 * @param tituloRectangulo T�tulo que aparece en el recuadro
	 * @return Objeto de tipo PdfPCell con el recuadro 
	 * @throws DocumentException Excepci�n generada en caso que se presente un problema insertando en el documento
	 */
	public static PdfPCell generarRecuadroExtendido(EncabezadoSEC encabezado, int anchoColumnas[], String tituloRectangulo) throws DocumentException{
		RoundBR round = new RoundBR();
		round.setRadius(12);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable outter = new PdfPTable(1);
		outter.setWidths(anchoColumnas);
	    
	    Phrase phrase = new Phrase(tituloRectangulo, FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
	    PdfPCell cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    //cell.setColspan(1);
	    cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
	    outter.addCell(cell);
	    
	    phrase = new Phrase(encabezado.getRecuadro().getNumOperacion()!=null?encabezado.getRecuadro().getNumOperacion():"", FontStyleSEC.TIMES_ROMAN_BOLD_RED_14PTS);
	    cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    //cell.setColspan(3);
	    cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
	    outter.addCell(cell);
	    
	    cell = new PdfPCell(outter);
	    cell.setPadding(4);
	    cell.setCellEvent(roundRectangle);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setVerticalAlignment(Chunk.ALIGN_TOP);
	    
		return cell;
	}
	
	/**
	 * M�todo encargado de generar un recuadro solo con t�tulo
	 * @param anchoColumnas Ancho en pixeles de cada columna
	 * @param tituloRectangulo T�tulo que aparece en el recuadro
	 * @return Objeto de tipo PdfPCell con el recuadro 
	 * @throws DocumentException Excepci�n generada en caso que se presente un problema insertando en el documento
	 */
	public static PdfPCell generarRecuadroBasico(int anchoColumnas[], String tituloRectangulo) throws DocumentException{
		RoundBR round = new RoundBR();
		round.setRadius(12);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable outter = new PdfPTable(1);
		outter.setWidths(anchoColumnas);
	    
	    Phrase phrase = new Phrase(tituloRectangulo, FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
	    PdfPCell cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setColspan(3);
	    cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
	    outter.addCell(cell);
	    
	    cell = new PdfPCell(outter);
	    cell.setPadding(4);
	    cell.setCellEvent(roundRectangle);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setVerticalAlignment(Chunk.ALIGN_TOP);
	    
		return cell;
	}
	
	/**
	 * M�todo encargado de generar un recuadro solo con t�tulo
	 * @param anchoColumnas Ancho en pixeles de cada columna
	 * @param tituloRectangulo T�tulo que aparece en el recuadro
	 * @return Objeto de tipo PdfPCell con el recuadro 
	 * @throws DocumentException Excepci�n generada en caso que se presente un problema insertando en el documento
	 */
	public static PdfPCell generarRecuadroF4(int anchoColumnas[], String tituloRectangulo) throws DocumentException{
		RoundBR round = new RoundBR();
		round.setRadius(8);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable outter = new PdfPTable(1);
		outter.setWidths(anchoColumnas);
	    
	    Phrase phrase = new Phrase(tituloRectangulo, FontStyleSEC.HELVETICA_7PTS);
	    PdfPCell cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setColspan(3);
	    cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
	    outter.addCell(cell);
	    
	    cell = new PdfPCell(outter);
	    cell.setPadding(4);
	    cell.setCellEvent(roundRectangle);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setVerticalAlignment(Chunk.ALIGN_TOP);
	    
		return cell;
	}
	
	public static PdfPCell generarRecuadro(PdfPTable tabla, int padding, int radio) throws DocumentException{
		RoundBR round = new RoundBR();
		round.setRadius(radio);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable outter = new PdfPTable(1);
		
	    PdfPCell cell = new PdfPCell(tabla);
	    cell.setBorder(Rectangle.NO_BORDER);
	    outter.addCell(cell);
	    
	    cell = new PdfPCell(outter);
	    cell.setPadding(padding);
	    cell.setCellEvent(roundRectangle);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setVerticalAlignment(Chunk.ALIGN_TOP);
	    
		return cell;
	}
	
	/**
	 * M�todo encargado de generar un recuadro para el tipo de operaci�n Inicial usuario Interno
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param cantidadColumnas Cantidad de columnas del recuadro
	 * @param anchoColumnas Ancho en pixeles de cada columna
	 * @param tituloRectangulo T�tulo que aparece en el recuadro
	 * @return Objeto de tipo PdfPCell con el recuadro 
	 * @throws DocumentException Excepci�n generada en caso que se presente un problema insertando en el documento
	 */
    public static PdfPCell generarRecuadroInternoInicial(EncabezadoSEC encabezado, int cantidadColumnas, int anchoColumnas[], String tituloRectangulo) throws DocumentException{
		
		RoundBR round = new RoundBR();
		round.setRadius(12);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable innertable = new PdfPTable(cantidadColumnas);
	    innertable.setWidths(anchoColumnas);
	    
	    // fila uno
        // column 1
        
        Phrase phrase = new Phrase(tituloRectangulo, FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        PdfPCell cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
        innertable.addCell(cell);
        
        // fila uno
        // column 1
        phrase = new Phrase();
        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        innertable.addCell(cell);
        
	    // fila dos
        // column 1
        phrase = new Phrase("Fecha Radicaci�n:", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        //columna 2
        phrase = new Phrase(encabezado.getRecuadro().getFechaRadicacion()!=null?encabezado.getRecuadro().getFechaRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        //fila 2
        //columna 1
        phrase = new Phrase("N�mero Radicaci�n:", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        //columna 2
        phrase = new Phrase(encabezado.getRecuadro().getNumeroRadicacion()!=null?encabezado.getRecuadro().getNumeroRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        cell = new PdfPCell(innertable);
        cell.setPadding(4);
        cell.setCellEvent(roundRectangle);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
        
		return cell;
	}
    
    /**
	 * M�todo encargado de generar un recuadro cuando no se selecciona operaci�n con usuario Interno
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param cantidadColumnas Cantidad de columnas del recuadro
	 * @param anchoColumnas Ancho en pixeles de cada columna
	 * @param tituloRectangulo T�tulo que aparece en el recuadro
	 * @return Objeto de tipo PdfPCell con el recuadro 
	 * @throws DocumentException Excepci�n generada en caso que se presente un problema insertando en el documento
	 */
    public static PdfPCell generarRecuadroInternoSinOperacion(EncabezadoSEC encabezado, int cantidadColumnas, int anchoColumnas[], String tituloRectangulo) throws DocumentException{
		
		RoundBR round = new RoundBR();
		round.setRadius(12);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable innertable = new PdfPTable(cantidadColumnas);
	    innertable.setWidths(anchoColumnas);
	    
	    // fila uno
        // column 1
        
        Phrase phrase = new Phrase(tituloRectangulo, FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        PdfPCell cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
        innertable.addCell(cell);
        
        // fila uno
        // column 1
        phrase = new Phrase();
        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        innertable.addCell(cell);
        
	    // fila dos
        // column 1
        phrase = new Phrase("", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        //columna 2
        phrase = new Phrase(encabezado.getRecuadro().getFechaRadicacion()!=null?encabezado.getRecuadro().getFechaRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        //fila 2
        //columna 1
        phrase = new Phrase("", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        //columna 2
        phrase = new Phrase(encabezado.getRecuadro().getNumeroRadicacion()!=null?encabezado.getRecuadro().getNumeroRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        
        cell = new PdfPCell(innertable);
        cell.setPadding(4);
        cell.setCellEvent(roundRectangle);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
        
		return cell;
	}
    
    /**
	 * M�todo encargado de generar un recuadro con operaci�n para el tipo de operaci�n Inicial y con usuario interno
	 * @param encabezado Objeto que contiene los datos necesarios para generar el encabezado
	 * @param cantidadColumnas Cantidad de columnas del recuadro
	 * @param anchoColumnas Ancho en pixeles de cada columna
	 * @param tituloRectangulo T�tulo que aparece en el recuadro
	 * @return Objeto de tipo PdfPCell con el recuadro 
	 * @throws DocumentException Excepci�n generada en caso que se presente un problema insertando en el documento
	 */
	public static PdfPCell generarRecuadroInternoConOperacion(EncabezadoSEC encabezado, int cantidadColumnas, int anchoColumnas[], String tituloRectangulo) throws DocumentException{
		RoundBR round = new RoundBR();
		//round.setRadius(12);
		PdfPCellEvent roundRectangle = new RoundRectangle(round);
		PdfPTable outter = new PdfPTable(cantidadColumnas);
		outter.setWidths(anchoColumnas);
	    
	    Phrase phrase = new Phrase(tituloRectangulo, FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
	    PdfPCell cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setColspan(3);
	    cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
	    outter.addCell(cell);
	    
	    phrase = new Phrase();
	    cell = new PdfPCell(phrase);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setColspan(3);
	    cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
	    outter.addCell(cell);
		
	    
	    PdfPTable inner1 = new PdfPTable(1);
	    phrase = new Phrase("Fecha Radicaci�n:", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
	    cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        
	    inner1.addCell(cell);
	    
	    phrase = new Phrase("N�mero Radicaci�n:", FontStyleSEC.TIMES_ROMAN_BOLD_7PTS);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setBorder(Rectangle.NO_BORDER);
        inner1.addCell(cell);
        
        
	    PdfPTable inner2 = new PdfPTable(1);
	    
	    phrase = new Phrase(encabezado.getRecuadro().getFechaRadicacion()!=null?encabezado.getRecuadro().getFechaRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setColspan(1);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        inner2.addCell(cell);
        
        phrase = new Phrase(encabezado.getRecuadro().getNumeroRadicacion()!=null?encabezado.getRecuadro().getNumeroRadicacion():"");
        phrase.getFont().setSize(FontStyleSEC.NORMAL_SIZE);
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        inner2.addCell(cell);
	    
        PdfPTable inner3 = new PdfPTable(1);
        
	    phrase = new Phrase(encabezado.getRecuadro().getNumOperacion()!=null?encabezado.getRecuadro().getNumOperacion():"", FontStyleSEC.TIMES_ROMAN_BOLD_RED_9PTS);
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Chunk.ALIGN_LEFT);
        cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
        cell.setBorder(Rectangle.NO_BORDER);             
        inner3.addCell(cell);
        
        cell = new PdfPCell(inner1);
        cell.setBorder(Rectangle.NO_BORDER);
	    outter.addCell(cell);
	    cell = new PdfPCell(inner2);
        cell.setBorder(Rectangle.NO_BORDER);
	    outter.addCell(cell);
	    cell = new PdfPCell(inner3);
        cell.setBorder(Rectangle.NO_BORDER);
        outter.addCell(cell);
        
	    cell = new PdfPCell(outter);
	    cell.setPadding(4);
	    cell.setCellEvent(roundRectangle);
	    cell.setBorder(Rectangle.NO_BORDER);
	    cell.setVerticalAlignment(Chunk.ALIGN_MIDDLE);
	    
		return cell;
	}
}
