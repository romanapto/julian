package com.journaldev.mongodb.converter;

import org.bson.types.ObjectId;

import com.journaldev.mongodb.model.Person;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.websystique.springmvc.model.User;
/**
 * 
 * @author jaroman
 *
 */
public class UserConverter {


	/**
	 * se combierte de usuario a objeto para gestion en base de datos
	 * @param p usuario
	 * @return objeto de base de datos
	 */
	public static DBObject toDBObject(User p) {

		BasicDBObjectBuilder builder = BasicDBObjectBuilder.start()
				.append("user", p.getUsername()).append("address", p.getAddress()).append("mail", p.getEmail());
		if (p.getId() != null)
			builder = builder.append("_id", new ObjectId(p.getId()));
		return builder.get();
	}

	/**
	 * se combierte de objeto de base de datos a usuario
	 * @param doc
	 * @return usuario
	 */
	public static User toUser(DBObject doc) {
		User p = new User();
		p.setUsername((String) doc.get("user"));
		p.setAddress((String) doc.get("address"));
		p.setEmail((String) doc.get("mail"));
		ObjectId id = (ObjectId) doc.get("_id");
		p.setId(id.toString());
		return p;

	}

}
