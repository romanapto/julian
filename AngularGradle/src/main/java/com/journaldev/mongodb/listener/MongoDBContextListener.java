package com.journaldev.mongodb.listener;

import java.net.UnknownHostException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.journaldev.mongodb.utils.LectorPropiedades;
import com.journaldev.mongodb.utils.exceptions.ExcepcionConfiguracion;
import com.journaldev.mongodb.utils.properties.ConstantesGenerales;
import com.journaldev.mongodb.utils.properties.ConstantesPropiedades;
import com.mongodb.MongoClient;

@WebListener
public class MongoDBContextListener implements ServletContextListener {
	
	private final static Logger LOGGER = Logger.getLogger(MongoDBContextListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		MongoClient mongo = (MongoClient) sce.getServletContext()
							.getAttribute(ConstantesGenerales.MONGO_CLIENT);
		mongo.close();
		System.out.println("MongoClient closed successfully");
	}

	@Override
	public void contextInitialized(ServletContextEvent sce)  {
		String mongo_Port = new String();
		String mongo_Host = new String();
		try {
			mongo_Port = LectorPropiedades.getInstance().getPropiedades().getProperty(ConstantesGenerales.MONGO_PORT);
			mongo_Host = LectorPropiedades.getInstance().getPropiedades().getProperty(ConstantesGenerales.MONGODB_HOST);
			MongoClient mongo = new MongoClient(
					mongo_Host, 
					Integer.parseInt((mongo_Port)));
			System.out.println("MongoClient initialized successfully");
			sce.getServletContext().setAttribute(ConstantesGenerales.MONGO_CLIENT, mongo);
		}
		 catch (ExcepcionConfiguracion e) {
			ExceptionUtils.getStackTrace(e);
			LOGGER.error("Error estableciendo conexion " + e);
			throw new RuntimeException("Ocurri� un error al cargar el archivo de propiedades ubicado en: " , e);
		}
		catch (UnknownHostException e) {
			LOGGER.error("Error estableciendo conexion " + e);
			throw new RuntimeException("MongoClient init failed");
		}
		
	}

}
