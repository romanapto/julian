package com.journaldev.mongodb.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.journaldev.mongodb.utils.exceptions.ExcepcionConfiguracion;
import com.journaldev.mongodb.utils.properties.ConstantesPropiedades;

/**
 * 
 * @author jaroman
 *
 */
public final class LectorPropiedades {
	
	private volatile static LectorPropiedades lectorPropiedades;
	private Properties propiedades = null;
	private final static Logger LOGGER = Logger.getLogger(LectorPropiedades.class);

	public Properties getPropiedades()  {
		return propiedades;
	}
	
	public static final LectorPropiedades getInstance() throws ExcepcionConfiguracion {
		if(lectorPropiedades == null) {
			synchronized (LectorPropiedades.class) {
				if(lectorPropiedades == null) {
					lectorPropiedades = new LectorPropiedades();
				}
			}
		}
		return lectorPropiedades;
	}
	
	private LectorPropiedades() throws ExcepcionConfiguracion {
		String propiedadUbicacion = null;
		File rutaArchivoPropiedades = null;
		InputStream archivoUbicacion = null;
		InputStream archivoPropiedadesGenerales = null;
		ClassLoader loader = null;
		Properties propiedadesAplicacion = new Properties();
		propiedades = new Properties();
		
		try {
			/* Cargando el archivo de propiedades */
			loader = Thread.currentThread().getContextClassLoader();
			archivoUbicacion = loader.getResourceAsStream(ConstantesPropiedades.UBICACION_PROPERTIES);
			if(archivoUbicacion == null) {
				LOGGER.error("No se encontr� el archivo " + ConstantesPropiedades.UBICACION_PROPERTIES);
				throw new ExcepcionConfiguracion("No se encontr� el archivo " + ConstantesPropiedades.UBICACION_PROPERTIES);
			}
			propiedadesAplicacion.load(archivoUbicacion);
			propiedadUbicacion = propiedadesAplicacion.getProperty(ConstantesPropiedades.PROPIEDAD_UBICACION);
			if (propiedadUbicacion != null && !"".equals(propiedadUbicacion)) {
				rutaArchivoPropiedades = new File(propiedadUbicacion);
				LOGGER.debug("RUTA UBICACION PROPIEDADES GENERALES: " + propiedadUbicacion);
				archivoPropiedadesGenerales = new FileInputStream(rutaArchivoPropiedades);
				propiedades.load(archivoPropiedadesGenerales);
				
				Enumeration<Object> enumeracion = propiedadesAplicacion.keys();
				for(;enumeracion.hasMoreElements();) {
					String propiedad = (String) enumeracion.nextElement();
					if(propiedadesAplicacion.get(propiedad) != null) {
						propiedades.put(propiedad, propiedadesAplicacion.get(propiedad));
					}
				}
				cargarPropiedadVersion();
			} else {
				LOGGER.error("No se encontr� la propiedad " + ConstantesPropiedades.PROPIEDAD_UBICACION);
				throw new ExcepcionConfiguracion(
						"No se encontr� la propiedad " + ConstantesPropiedades.PROPIEDAD_UBICACION);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("Ocurri� un error al cargar el archivo de propiedades ubicado en: " + rutaArchivoPropiedades + " - " + 
					ExceptionUtils.getStackTrace(e));
			throw new ExcepcionConfiguracion(
					"Ocurri� un error al cargar el archivo de propiedades ubicado en: " + rutaArchivoPropiedades, e);
		} catch (IOException e) {
			LOGGER.error("Ocurri� un error al cargar el archivo de propiedades ubicado en: " + archivoUbicacion + " - " + 
					ExceptionUtils.getStackTrace(e));
			throw new ExcepcionConfiguracion(
					"Ocurri� un error al cargar el archivo de propiedades ubicado en: " + archivoUbicacion, e);
		} catch (SecurityException e) {
			LOGGER.error("No existen permisos para leer el archivo de propiedades ubicado en: " + rutaArchivoPropiedades + " - " + 
					ExceptionUtils.getStackTrace(e));
			throw new ExcepcionConfiguracion(
					"No existen permisos para leer el archivo de propiedades ubicado en: " + rutaArchivoPropiedades, e);
		}
		catch (Exception e) {
			LOGGER.error("Ocurri� un error al cargar el archivo de propiedades ubicado en: " + rutaArchivoPropiedades + " - " + 
					ExceptionUtils.getStackTrace(e));
			throw new ExcepcionConfiguracion(
					"Ocurri� un error al cargar el archivo de propiedades ubicado en: " + rutaArchivoPropiedades, e);
		}
	}
	
	private void cargarPropiedadVersion() throws Exception {
		InputStream archivoUbicacion = null;
		ClassLoader loader = null;
		Properties propiedadesAplicacion = new Properties();
		
		/* Cargando el archivo de propiedades */
		loader = Thread.currentThread().getContextClassLoader();
		archivoUbicacion = loader.getResourceAsStream(ConstantesPropiedades.PROPIEDAD_VERSION + ".properties");
		
		if(archivoUbicacion != null) {
			propiedadesAplicacion.load(archivoUbicacion);
			
			Enumeration<Object> enumeracion = propiedadesAplicacion.keys();
			for(;enumeracion.hasMoreElements();) {
				String propiedad = (String) enumeracion.nextElement();
				if(propiedadesAplicacion.get(propiedad) != null) {
					propiedades.put(propiedad, propiedadesAplicacion.get(propiedad));
				}
			}
		}
	}

}
