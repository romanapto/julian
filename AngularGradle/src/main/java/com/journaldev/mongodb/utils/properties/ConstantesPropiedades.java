/**
 * 
 */
package com.journaldev.mongodb.utils.properties;

/**
 * 
 * @author jaroman
 *
 */
public final class ConstantesPropiedades {
	
	public static final String UBICACION_PROPERTIES  = "custom_properties.properties";
	
	public static final String PROPIEDAD_UBICACION   = "ubicacion_archivo_propiedades";
	
	public static final String PROPIEDAD_VERSION 	 = "version_mon";

}
