package com.journaldev.mongodb.utils.exceptions;

/**
 * Inserte aqu� la descripci�n del tipo. Fecha de creaci�n: (20/08/2002 03:19:24 p.m.)
 * 
 * @author: Carlos Alejandro Santos
 */
public class ExcepcionConfiguracion extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2443356927615034043L;

	/**
	 * Comentario de constructor ExcepcionConfiguracion.
	 */
	public ExcepcionConfiguracion() {
		super();
	}

	/**
	 * Comentario de constructor ExcepcionConfiguracion.
	 * 
	 * @param s java.lang.String
	 */
	public ExcepcionConfiguracion(String s) {
		super(s);
	}
	
	/**
	 * Comentario de constructor ExcepcionConfiguracion.
	 * @param message
	 * @param Throwable
	 */
	public ExcepcionConfiguracion(String s, Throwable t) {
		super(s, t);
	}
}
