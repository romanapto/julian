/**
 * 
 */
package com.journaldev.mongodb.utils.properties;

/**
 * 
 * @author jaroman
 *
 */
public final class ConstantesGenerales {
	
	public static final String MONGO_PORT  = "MONGODB_PORT";
	public static final String MONGODB_HOST  = "MONGODB_HOST";
	public static final String MONGO_CLIENT  = "MONGO_CLIENT";
	
	
}
