package com.journaldev.mongodb.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import com.journaldev.mongodb.converter.PersonConverter;
import com.journaldev.mongodb.converter.UserConverter;
import com.journaldev.mongodb.model.Person;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.websystique.springmvc.model.User;
import com.websystique.springmvc.service.UserService;
/**
 * 
 * @author jaroman
 *
 */
public class MongoDBUserDAO {
	
	private static final AtomicLong counter = new AtomicLong();
	
	private DBCollection col;
	
	private boolean mongoUp;
	
	private String errorMensajeBd;
	
	@Autowired
    UserService userService;

	/**
	 * Se establece conexion a bd
	 * @param mongo
	 */
	public MongoDBUserDAO(MongoClient mongo) {
		this.col = mongo.getDB("Registration").getCollection("Users");
		try{
            DBObject ping = new BasicDBObject("ping", "1");
            mongo.getDB("Registration").command(ping);
            mongoUp=true;
        } catch (Exception exp){
            // MongoDb is down..
        	mongoUp=false;
        	errorMensajeBd = exp.getMessage();
        }
	}

	/**
	 * Se inserta en tabla de usuario, se crea usuario
	 * @param p usuario insertado
	 * @return
	 */
	public User createUser(User p) {
		DBObject doc = UserConverter.toDBObject(p);
		this.col.insert(doc);
		ObjectId id = (ObjectId) doc.get("_id");
		p.setId(id.toString());
		return p;
	}

	/**
	 * Se actualiza usuario
	 * @param p usuario actualizado
	 */
	public void updatePerson(User p) {
		DBObject query = BasicDBObjectBuilder.start()
				.append("_id", new ObjectId(p.getId())).get();
		this.col.update(query, UserConverter.toDBObject(p));
	}

	/**
	 * Se consulta usuarios
	 * @return lista con usuarios
	 */
	public List<User> readAllUser() {
		List<User> data = new ArrayList<User>();
		DBCursor cursor = col.find();
		if (mongoUp) {
		while (cursor.hasNext()) {
			DBObject doc = cursor.next();
			User p = UserConverter.toUser(doc);
			data.add(p);
		}
		} else {
			data.add(new User("Mongo its Down", "No hay conexion a la Base de datos", "Error: "+errorMensajeBd, ""));
		}
		return data;
	}

	/**
	 * se elimina usuario
	 * @param p usuario a eliminar
	 */
	public void deletePerson(Person p) {
		DBObject query = BasicDBObjectBuilder.start()
				.append("_id", new ObjectId(p.getId())).get();
		this.col.remove(query);
	}

	/**
	 * Se consulta usuario
	 * @param p
	 * @return
	 */
	public Person readPerson(Person p) {
		DBObject query = BasicDBObjectBuilder.start()
				.append("_id", new ObjectId(p.getId())).get();
		DBObject data = this.col.findOne(query);
		return PersonConverter.toPerson(data);
	}


}
